$('#smsgoback').on('tap',function(){
	mui.openWindow({
		url:'my.html'
	})
})
$(function(){
	$.ajax({
		url: "../json/countries.json", // 加随机数防止缓存
		type: "get",
		dataType: "json",
		success: function (data) {
			var html = '';
			var regex1 = /\((\d+)\)/;   // () 小括号
			for(var i = 0 ; i < data.length;i++){
				html+="<li class='arealist'>"+data[i].countryCode.displayValue+"<span class='arealistcontent' style='display:none'>"+data[i].countryCode.rawValue+"</span></li>";
			}
			document.getElementById("areaul").innerHTML = html;
			$(".arealist").each(function(){					
				$(this).on('tap',function(){
					var str = $(this).html();
					var strtwo = str.substring(str.indexOf("(")+1,str.indexOf(")"))
					$(".areanumber").html(strtwo);
					$(".areaselect").html($(this).find('.arealistcontent').html());
					document.getElementById('signareatips').style.left = '100%';
					//$(".SecretKey").fadeIn()
				})
			})
		}
		
	});
	document.getElementsByClassName("buttonsubmit")[0].addEventListener('tap', function() {
		//表单中的一个值
		var sendData = {} 
		var Nationality = $(".areaselect").html();
		var Mobilenumber = document.getElementById("Mobilenumber").value; 
		sendData["Nationality"] = Nationality
		sendData["Mobilenumber"] = Mobilenumber
		//校验输入框不为空,并且复选框为true的情况下
		if(Mobilenumber!=''){
			//提交部分数据部分
			mui.openWindow({ //目标页面
				url: 'DisableSMSAuthenticationConfirmEmailAftersendSMS.html',
				extras: {
					name: Nationality,
					version: Mobilenumber,
				}
			})
		}else{
			mui.toast(get_lan('Eenter_Phone_Number'));
			//输入框不符合要求的情况下进行处理 
		}
		
	}) 
	document.getElementById('smsareacode').addEventListener('tap',function(){
		document.getElementById('signareatips').style.left = '0';
	});
	document.getElementById('signareaback').addEventListener('tap',function(){
		document.getElementById('signareatips').style.left = '100%';
	});
})