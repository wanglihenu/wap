(function($, doc) {
	$.init();
	$.ready(function() {
		/**
		 * 获取对象属性的值
		 * 主要用于过滤三级联动中，可能出现的最低级的数据不存在的情况，实际开发中需要注意这一点；
		 * @param {Object} obj 对象
		 * @param {String} param 属性名
		 */
// 		var _getParam = function(obj, param) {
// 			return obj[param] || '';
// 		};
// 		//普通示例
// 		var userPicker = new $.PopPicker();
// 		userPicker.setData([{
// 			value: "ywy",
// 			text: "ETH"
// 		}, {
// 			value: 'aaa',
// 			text: "BTC"
// 		}, {
// 			value: 'lj',
// 			text: "ETH"
// 		}, {
// 			value: 'ymt',
// 			text: "USDT"
// 		}]);
// 		var showUserPickerButtonone = doc.getElementById('refineboxcoinone');
// 		var showUserPickerButtontwo = doc.getElementById('refineboxcointwo');
// 		var showUserPickerButtonthree = doc.getElementById('refineboxtwocoinone');
// 		var showUserPickerButtonfour = doc.getElementById('refineboxtwocointwo');
// 		showUserPickerButtonone.addEventListener('tap', function(event) {
// 			userPicker.show(function(items) {
// 				showUserPickerButtonone.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "")
// 				//返回 false 可以阻止选择框的关闭
// 				//return false;
// 			});
// 		}, false);
// 		showUserPickerButtontwo.addEventListener('tap', function(event) {
// 			userPicker.show(function(items) {
// 				showUserPickerButtontwo.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "")
// 				//返回 false 可以阻止选择框的关闭
// 				//return false;
// 			});
// 		}, false);
// 		showUserPickerButtonthree.addEventListener('tap', function(event) {
// 			userPicker.show(function(items) {
// 				showUserPickerButtonthree.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "")
// 				//返回 false 可以阻止选择框的关闭
// 				//return false;
// 			});
// 		}, false);
// 		showUserPickerButtonfour.addEventListener('tap', function(event) {
// 			userPicker.show(function(items) {
// 				showUserPickerButtonfour.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "")
// 				//返回 false 可以阻止选择框的关闭
// 				//return false;
// 			});
// 		}, false);
// 	var result = $('#result')[0];
// 	var refinedateone = document.getElementById('refinedateone');
// 	var refinedatetwo = document.getElementById('refinedatetwo');
	//btns.each(function(i, btn) {
// 		refinedateone.addEventListener('tap', function(){
// 			var _self = this;
// 			if(_self.picker) {
// 				_self.picker.show(function (rs) {
// 					refinedateone.innerText = rs.text;
// 					_self.picker.dispose();
// 					_self.picker = null;
// 				});
// 			} else {
// 				var optionsJson = this.getAttribute('data-options') || '{}';
// 				var options = JSON.parse(optionsJson);
// 				var id = this.getAttribute('id');
// 				/*
// 				 * 首次显示时实例化组件
// 				 * 示例为了简洁，将 options 放在了按钮的 dom 上
// 				 * 也可以直接通过代码声明 optinos 用于实例化 DtPicker
// 				 */
// 				_self.picker = new $.DtPicker(options);
// 				_self.picker.show(function(rs) {
// 					/*
// 					 * rs.value 拼合后的 value
// 					 * rs.text 拼合后的 text
// 					 * rs.y 年，可以通过 rs.y.vaue 和 rs.y.text 获取值和文本
// 					 * rs.m 月，用法同年
// 					 * rs.d 日，用法同年
// 					 * rs.h 时，用法同年
// 					 * rs.i 分（minutes 的第二个字母），用法同年
// 					 */
// 					refinedateone.innerText =rs.text;
// 					/* 
// 					 * 返回 false 可以阻止选择框的关闭
// 					 * return false;
// 					 */
// 					/*
// 					 * 释放组件资源，释放后将将不能再操作组件
// 					 * 通常情况下，不需要示放组件，new DtPicker(options) 后，可以一直使用。
// 					 * 当前示例，因为内容较多，如不进行资原释放，在某些设备上会较慢。
// 					 * 所以每次用完便立即调用 dispose 进行释放，下次用时再创建新实例。
// 					 */
// 					_self.picker.dispose();
// 					_self.picker = null;
// 				});
// 			}
// 			
// 		}, false);
// 		refinedatetwo.addEventListener('tap', function(){
// 			var _self = this;
// 			if(_self.picker) {
// 				_self.picker.show(function (rs) {
// 					refinedatetwo.innerText = rs.text;
// 					_self.picker.dispose();
// 					_self.picker = null;
// 				});
// 			} else {
// 				var optionsJson = this.getAttribute('data-options') || '{}';
// 				var options = JSON.parse(optionsJson);
// 				var id = this.getAttribute('id');
// 				/*
// 				 * 首次显示时实例化组件
// 				 * 示例为了简洁，将 options 放在了按钮的 dom 上
// 				 * 也可以直接通过代码声明 optinos 用于实例化 DtPicker
// 				 */
// 				_self.picker = new $.DtPicker(options);
// 				_self.picker.show(function(rs) {
// 					/*
// 					 * rs.value 拼合后的 value
// 					 * rs.text 拼合后的 text
// 					 * rs.y 年，可以通过 rs.y.vaue 和 rs.y.text 获取值和文本
// 					 * rs.m 月，用法同年
// 					 * rs.d 日，用法同年
// 					 * rs.h 时，用法同年
// 					 * rs.i 分（minutes 的第二个字母），用法同年
// 					 */
// 					refinedatetwo.innerText =rs.text;
// 					/* 
// 					 * 返回 false 可以阻止选择框的关闭
// 					 * return false;
// 					 */
// 					/*
// 					 * 释放组件资源，释放后将将不能再操作组件
// 					 * 通常情况下，不需要示放组件，new DtPicker(options) 后，可以一直使用。
// 					 * 当前示例，因为内容较多，如不进行资原释放，在某些设备上会较慢。
// 					 * 所以每次用完便立即调用 dispose 进行释放，下次用时再创建新实例。
// 					 */
// 					_self.picker.dispose();
// 					_self.picker = null;
// 				});
// 			}
// 			
// 		}, false);
	});
})(mui, document);
$(function(){
	mui.getExtras(function(extras){
	   if(extras.type == "open"){
		   $('.orderstitleopen').css('display','block');
		   $('.orderstitlehistory').css('display','none');
		   $('#tabone_content').css('display','block'); 
		   $('#tabtwo_content').css('display','none');
		   $(".tabtwo").removeClass("tabactive")
		   $(".tabone").addClass("tabactive")
	   }else if(extras.type == "history"){
		   $('.orderstitleopen').css('display','none');
		   $('.orderstitlehistory').css('display','block');
		   $('#tabone_content').css('display','none'); 
		   $('#tabtwo_content').css('display','block');
		   $(".tabone").removeClass("tabactive")
		   $(".tabtwo").addClass("tabactive")
	   }
	});
	$.ajax({
		url:"https://rest.glenbit.com/order/open",
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success:function(data){
			loginstatus(data)
			if(data.errors != undefined){
				mui.toast(data.errors[0].msg)
				return;
			}
			var html = '';
			function sortId(a,b){  
			   return b.createdAt-a.createdAt  
			}
			data.sort(sortId);
			
			if(data == ''){
				$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
			}else{
				for(var i = 0 ; i < data.length ; i++){
				if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('lan') == null){
					if(data[i].type == 'buy'){
						data[i].type = '买入'
					}else if(data[i].type == 'sell'){
						data[i].type = '卖出'
					}
				}
				data[i].pair = data[i].pair.replace(/\-/g, "/");
				var now = new Date(parseInt(data[i].createdAt));
				y = now.getFullYear();
				m = ("0" + (now.getMonth() + 1)).slice(-2);
				d = ("0" + now.getDate()).slice(-2);
				html +='<li class="refinelist">'+
					'<div class="refinelistcoontent">'+
					'<div class="refineli_date">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</div>'+
					'<div class="refineli_left">'+
						'<p class = "refinelipair">'+data[i].pair+'</p>'+
						'<span class="refinelitype">'+data[i].type+'</span>'+
					'</div>'+
					'<div class="refineli_right">'+
						'<span class="righttop">'+data[i].price+'</span>'+
						'<p>'+data[i].amount+'</p>'+
					'</div>'+
					'<div class="refinearrow"></div>'+
					'<div class="refinearrowactive"></div>'+
					'<span class="clicknum" style="display: none;">'+data[i].orderId+'</span>'+
					'</div>'+
					'<div class="refineulcancel"><div class="refineulcancelbtn">'+get_lan("CANCEL")+'</div></div>'
				'</li>'
				}
				$('#refineul').html(html)
			}
			
			$("#refineul .refinelistcoontent").each(function(){
				$(this).on('tap',function(){ 
					var $pth = $(this).parent(".refinelistcoontent").siblings(".refineulcancel"); 
// 					if($(this).find('.refinearrow').hasClass("refinearrowactive")){
// 						$(this).find('.refinearrow').addClass("refinearrow")
// 						$(this).find('.refinearrow').removeClass("refinearrowactive") 
// 						//$pth.fadeOut();
// 						//$pth.fadeOut(); 
// 					}else if($(this).find('.refinearrow').hasClass("refinearrow")){ 
// 						$(this).find('.refinearrow').removeClass("refinearrow");
// 						$(this).find('.refinearrow').addClass("refinearrowactive");
// 						//$pth.fadeIn();
// 						//$pth.fadeIn(); 
// 					} 
					$(this).siblings('.refineulcancel').slideToggle(200);
					$(this).find('.refinearrow').toggle(0);
					$(this).find('.refinearrowactive').toggle(0);
				})
			})
		}
	});
})
$('body').on('tap','.refineulcancelbtn',function(){
	var refinelipair = $(this).parent('.refineulcancel').siblings('.refinelistcoontent').find('.refinelipair').html();
	var type = $(this).parent('.refineulcancel').siblings('.refinelistcoontent').find('.refinelitype').html();
	var orderId = $(this).parent('.refineulcancel').siblings('.refinelistcoontent').find('.clicknum').html();
	refinelipair = refinelipair.replace(/\//g, "-")
	$.ajax({
		url: 'https://rest.glenbit.com/order/cancel',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		data: {
			pair: refinelipair,
			orderId: orderId,
			type: type,
		},
		success: (data) =>  {
			loginstatus(data)
			if(data.errors == undefined){
				$(this).parent('.refineulcancel').parent('.refinelist').remove();
				if( $('#refineul li').length == 0){
					$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
				}
			}
			
		}
	})
		
});
// document.getElementById('tabc_0').addEventListener('tap', function() {
// 	this.classList.add('tabactive');
// 	document.getElementById('tabc_1').classList.remove('tabactive');
// 	document.getElementById('tabone_content').style.display = 'block';
// 	document.getElementById('tabtwo_content').style.display = 'none';
// });
// document.getElementById('tabc_1').addEventListener('tap', function() {
// 	this.classList.add('tabactive');
// 	document.getElementById('tabc_0').classList.remove('tabactive');
// 	document.getElementById('tabone_content').style.display = 'none';
// 	document.getElementById('tabtwo_content').style.display = 'block';
// });
///
document.getElementById('tabonecoinbox').addEventListener('tap', function() {
	document.getElementById('refineboxone').style.left = '0';	
});
document.getElementById('tabonetypebox').addEventListener('tap', function() {
	document.getElementById('refineboxone').style.left = '0';	
});
//
document.getElementById('refineboxonegoback').addEventListener('tap', function() {
	document.getElementById('refineboxone').style.left = '100%';
});
document.getElementById('tabtwocoinbox').addEventListener('tap', function() {
	document.getElementById('refineboxtwo').style.left = '0';
});
document.getElementById('tabtwotypebox').addEventListener('tap', function() {
	document.getElementById('refineboxtwo').style.left = '0';
});
///
document.getElementById('refineboxtwogoback').addEventListener('tap', function() {
	document.getElementById('refineboxtwo').style.left = '100%';
	//document.getElementById('refineboxtwo').style.display = 'none';
});

//实例化筛选
var exchangecoinsarr = [];
var exchangemarketsarr = [];
var exchangecombination = [];
var exchangedefault = [];
var cionsmix = [];
var Marketmix = [];
$.ajax({
	url:'https://rest.glenbit.com/markets',
	type:'get',
	dataType:'json',
	xhrFields: {
		withCredentials: true
	},
	crossDomain: true,
	success:function(data){
		for(var i = 0; i <data.length; i++){
			var exchangemarketsjson = {};
			var exchangecoinsjson = {};
			var exchangecombinationjson = {};
			exchangecombinationjson.text = data[i].targetCoin;
			exchangecombinationjson.market = data[i].marketCoin;
			exchangecombination.push(exchangecombinationjson);
			exchangecoinsjson.text = data[i].targetCoin;
			exchangecoinsarr.push(exchangecoinsjson);
			exchangemarketsjson.text = data[i].marketCoin;
			exchangemarketsarr.push(exchangemarketsjson);
		}
		var hash = {};
		marketarr = exchangemarketsarr.reduce(function(item, next) {
			hash[next.text] ? '' : hash[next.text] = true && item.push(next);
			return item
		}, [])
		for(var i = 0; i < exchangecombination.length ; i++){
			if('BTC' == exchangecombination[i].text){
				var marketselectdefault = {};
				marketselectdefault.text = exchangecombination[i].market;
				
				exchangedefault.push(marketselectdefault);
			}	
		}
		var hashtwo = {};
		currencyarr = exchangecoinsarr.reduce(function(item, next) {
			hashtwo[next.text] ? '' : hashtwo[next.text] = true && item.push(next);
			return item
		}, [])
		var _getParam = function(obj, param) {
			return obj[param] || '';
		};
		//普通示例
		var userPicker = new mui.PopPicker();
		var userPickertwo = new mui.PopPicker();
		var userPickertype = new mui.PopPicker();
		userPicker.setData(currencyarr);
		
		var currencychoice = document.getElementById('refineboxcoinone');
		var marketchoice = document.getElementById('refineboxcointwo');
		var refineboxtwocoinone = document.getElementById('refineboxtwocoinone');
		var refineboxtwocointwo = document.getElementById('refineboxtwocointwo');
		var refineboxtype = document.getElementById('refineboxtype');
		var refineboxtwotype = document.getElementById('refineboxtwotype');
		currencychoice.addEventListener('tap', function(event) {			
			userPicker.show(function(items) {
				marketarr = [];
				//cionsmix = [];
				var selectcions = JSON.stringify(items[0].text).replace(/\"/g, "");
				cionsmix.push(selectcions);
				for(var i = 0; i < exchangecombination.length ; i++){
					if(selectcions == exchangecombination[i].text){
						var marketselect = {};
						marketselect.text = exchangecombination[i].market;
						marketarr.push(marketselect);
					}
				}
				userPickertwo.setData(marketarr);
				currencychoice.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "");
				marketchoice.innerHTML = get_lan("choose");
				//返回 false 可以阻止选择框的关闭
				//return false;
			});
		}, false);
		userPickertwo.setData(exchangedefault);
		marketchoice.addEventListener('tap', function(event) {
			if(cionsmix == ''){
				//mui.toast('请先选择币种');
			}else{
				userPickertwo.show(function(items) {
					var marketchoiceselect = JSON.stringify(items[0].text).replace(/\"/g, "");
						Marketmix = [];
						Marketmix.push(marketchoiceselect);
						//cionsmix.push.apply(cionsmix,Marketmix);
						//console.log(cionsmix +','+ Marketmix)
						//$('.buytypename').html(cionsmix);
						//$('.buyneedcurrency').html(Marketmix);
						//acquiringcurrency(cionsmix,Marketmix);
						//wsexchange = cionsmix + '-' + Marketmix;
						//ws.close();
						//wsUrl = "wss://stream-main.glenbit.com/" + wsexchange;
						//console.log(wsUrl);
					marketchoice.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "");
					//返回 false 可以阻止选择框的关闭
					//return false;
				});
			}
			
		}, false);
		userPickertwo.setData(exchangedefault);
		refineboxtwocoinone.addEventListener('tap', function(event) {
			//console.log(currencyarr)
			userPicker.show(function(items) {
				marketarr = [];
				cionsmix = [];
				var selectcions = JSON.stringify(items[0].text).replace(/\"/g, "");
				cionsmix.push(selectcions);
				for(var i = 0; i < exchangecombination.length ; i++){
					if(selectcions == exchangecombination[i].text){
						var marketselect = {};
						marketselect.text = exchangecombination[i].market;
						marketarr.push(marketselect);
					}
				}
				userPickertwo.setData(marketarr);
				refineboxtwocoinone.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "");
				refineboxtwocointwo.innerHTML = get_lan("choose");
				//返回 false 可以阻止选择框的关闭
				//return false;
			});
		}, false);
		refineboxtwocointwo.addEventListener('tap', function(event) {
			if(cionsmix == ''){
				//mui.toast('请先选择币种');
			}else{
				userPickertwo.show(function(items) {
					var marketchoiceselect = JSON.stringify(items[0].text).replace(/\"/g, "");
						Marketmix = [];
						Marketmix.push(marketchoiceselect);
						//cionsmix.push.apply(cionsmix,Marketmix);
						//console.log(cionsmix +','+ Marketmix)
						//$('.buytypename').html(cionsmix);
						//$('.buyneedcurrency').html(Marketmix);
						//acquiringcurrency(cionsmix,Marketmix);
						//wsexchange = cionsmix + '-' + Marketmix;
						//ws.close();
						//wsUrl = "wss://stream-main.glenbit.com/" + wsexchange;
						//console.log(wsUrl);
					refineboxtwocointwo.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "");
					//返回 false 可以阻止选择框的关闭
					//return false;
				});
			}
			
		}, false);
		
		if(localStorage.getItem('lan') == 'en'){
			userPickertype.setData([
				{text:'ALL'},
				{text:'BUY'},
				{text:'SELL'}
			]);
		}else{
			userPickertype.setData([
				{text:'全部'},
				{text:'买'},
				{text:'卖'}
			]);
		}
		
		refineboxtype.addEventListener('tap', function(event) {
				userPickertype.show(function(items){
						//cionsmix.push.apply(cionsmix,Marketmix);
						//console.log(cionsmix +','+ Marketmix)
						//$('.buytypename').html(cionsmix);
						//$('.buyneedcurrency').html(Marketmix);
						//acquiringcurrency(cionsmix,Marketmix);
						//ws.close();
						//wsUrl = "wss://stream-main.glenbit.com/" + wsexchange;
						//console.log(wsUrl);
					refineboxtype.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "");
					//返回 false 可以阻止选择框的关闭
					//return false;
				});			
		}, false);
		refineboxtwotype.addEventListener('tap', function(event) {
				userPickertype.show(function(items){
						//cionsmix.push.apply(cionsmix,Marketmix);
						//console.log(cionsmix +','+ Marketmix)
						//$('.buytypename').html(cionsmix);
						//$('.buyneedcurrency').html(Marketmix);
						//acquiringcurrency(cionsmix,Marketmix);
						//ws.close();
						//wsUrl = "wss://stream-main.glenbit.com/" + wsexchange;
						//console.log(wsUrl);
					refineboxtwotype.innerHTML = JSON.stringify(items[0].text).replace(/\"/g, "");
					//返回 false 可以阻止选择框的关闭
					//return false;
				});			
		}, false);
		
	}
})
//
			
			$("#refineultwo .refinearrowtwo").each(function(){
				$(this).on('tap',function(){ 
					var $pth = $(this).parent(".refinelistcoontent").siblings(".refineulcancel"); 
					if($(this).hasClass("refinearrowactive")){
						$(this).addClass("refinearrowtwo")
						$(this).removeClass("refinearrowactive") 
						$pth.fadeOut();
						$pth.fadeOut(); 
					}else if($(this).hasClass("refinearrowtwo")){ 
						$(this).removeClass("refinearrowtwo");
						$(this).addClass("refinearrowactive");
						$pth.fadeIn();
						$pth.fadeIn(); 
					} 
				})
			})
			///
	//当前委托订单筛选
	$('#refinesubmit').on('tap',function(){
		var refineboxcoinone = $('#refineboxcoinone').html();
		var refineboxcointwo = $('#refineboxcointwo').html();
		var refineboxcointype = $('#refineboxtype').html();
		var refinesubmit = refineboxcoinone + '-' + refineboxcointwo;
		var refinename = refineboxcoinone + '/' + refineboxcointwo;
		if(refineboxcoinone == 'ALL' || refineboxcoinone == '所有' || refineboxcointwo == 'ALL' || refineboxcointwo == '所有' || refineboxcointwo == 'Choose' || refineboxcointwo == '选择'){
			mui.toast(get_lan('FAILURE'))
			return;
		}
		
		$.ajax({
		    url: 'https://rest.glenbit.com/order/open/' + refinesubmit,
		    type: 'POST',
		    dataType: 'json',
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			success:(function (data) {
				loginstatus(data)
				var html = '';
				
				if(refineboxcointype == '买' || refineboxcointype == 'BUY'){
					var buynumber = 0;
					var type="";
					for( i in data){
						if(data[i].type == 'buy'){
							buynumber++;
							
							var now = new Date(parseInt(data[i].createdAt));
							y = now.getFullYear();
							m = ("0" + (now.getMonth() + 1)).slice(-2);
							d = ("0" + now.getDate()).slice(-2);
							html +='<li class="refinelist">'+
								'<div class="refinelistcoontent">'+
								'<div class="refineli_date">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</div>'+
								'<div class="refineli_left">'+
									'<p class = "refinelipair">'+refinename+'</p>'+
									'<span class="refinelitype">'+get_lan('BUY')+'</span>'+
								'</div>'+
								'<div class="refineli_right">'+
									'<span class="righttop">'+data[i].price+'</span>'+
									'<p>'+data[i].amount+'</p>'+
								'</div>'+
								'<div class="refinearrow"></div>'+
								'<span class="clicknum" style="display: none;">'+data[i].orderId+'</span>'+
								'</div>'+
								'<div class="refineulcancel"><div class="refineulcancelbtn">'+get_lan("CANCEL")+'</div></div>'
							'</li>'
						}
					}
					if(buynumber == 0){
						$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
					}else{
						$('#refineul').html(html)
					}

					
				}else if(refineboxcointype == '卖' || refineboxcointype == 'SELL'){
					var sellnumber = 0;
					for( i in data){
						if(data[i].type == 'sell'){
							sellnumber++;
							var now = new Date(parseInt(data[i].createdAt));
							y = now.getFullYear();
							m = ("0" + (now.getMonth() + 1)).slice(-2);
							d = ("0" + now.getDate()).slice(-2);
							html +='<li class="refinelist">'+
								'<div class="refinelistcoontent">'+
								'<div class="refineli_date">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</div>'+
								'<div class="refineli_left">'+
									'<p class = "refinelipair">'+refinename+'</p>'+
									'<span class="refinelitype">'+get_lan('SELL')+'</span>'+
								'</div>'+
								'<div class="refineli_right">'+
									'<span class="righttop">'+data[i].price+'</span>'+
									'<p>'+data[i].amount+'</p>'+
								'</div>'+
								'<div class="refinearrow"></div>'+
								'<span class="clicknum" style="display: none;">'+data[i].orderId+'</span>'+
								'</div>'+
								'<div class="refineulcancel"><div class="refineulcancelbtn">'+get_lan("CANCEL")+'</div></div>'
							'</li>'
						}
					}
					if(sellnumber == 0){
						$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
					}else{
						$('#refineul').html(html)
					}
				}else if(data != ''){
					for( i in data){
						if(data[i].type == 'sell'){
							var now = new Date(parseInt(data[i].createdAt));
							y = now.getFullYear();
							m = ("0" + (now.getMonth() + 1)).slice(-2);
							d = ("0" + now.getDate()).slice(-2);
							html +='<li class="refinelist">'+
								'<div class="refinelistcoontent">'+
								'<div class="refineli_date">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</div>'+
								'<div class="refineli_left">'+
									'<p class = "refinelipair">'+refinename+'</p>'+
									'<span class="refinelitype">'+data[i].type+'</span>'+
								'</div>'+
								'<div class="refineli_right">'+
									'<span class="righttop">'+data[i].price+'</span>'+
									'<p>'+data[i].amount+'</p>'+
								'</div>'+
								'<div class="refinearrow"></div>'+
								'<span class="clicknum" style="display: none;">'+data[i].orderId+'</span>'+
								'</div>'+
								'<div class="refineulcancel"><div class="refineulcancelbtn">'+get_lan("CANCEL")+'</div></div>'
							'</li>'
						}
					}
					$('#refineul').html(html)
				}else if(data == ''){
					$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
				}	
				$('#currentone').html(refineboxcoinone);
				$('#currenttwo').html(refineboxcointwo);
				$('#currenttype').html(refineboxcointype);
				document.getElementById('refineboxone').style.left = '100%';
				$("#refineul .refinearrow").each(function(){
					$(this).on('tap',function(){ 
						var $pth = $(this).parent(".refinelistcoontent").siblings(".refineulcancel"); 
						if($(this).hasClass("refinearrowactive")){
							$(this).addClass("refinearrow")
							$(this).removeClass("refinearrowactive") 
							$pth.fadeOut();
							$pth.fadeOut(); 
						}else if($(this).hasClass("refinearrow")){ 
							$(this).removeClass("refinearrow");
							$(this).addClass("refinearrowactive");
							$pth.fadeIn();
							$pth.fadeIn(); 
						} 
					})
				})
			}),
			error:function(data){
				mui.toast(get_lan('FAILURE'))
			}
		})
	})
	//档当前委托重置
	$('#refineboxreset').on('tap',function(){
		
		$('#refineboxcoinone').html(get_lan('all'));
		$('#refineboxcointwo').html(get_lan('all'));
		$('#refineboxtype').html(get_lan('all'));
		$('#currentone').html('-');
		$('#currenttwo').html('-');
		$('#currenttype').html('- -');
		$.ajax({
			url:"https://rest.glenbit.com/order/open",
			type: 'POST',
			dataType: 'json',
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			success:function(data){
				loginstatus(data)
				if(data.errors != undefined){
					mui.toast(data.errors[0].msg)
					return;
				}
				var html = '';
				function sortId(a,b){  
				   return b.createdAt-a.createdAt  
				}
				data.sort(sortId);
				
				if(data == ''){
					$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
				}else{
					for(var i = 0 ; i < data.length ; i++){
					var now = new Date(parseInt(data[i].createdAt));
					y = now.getFullYear();
					m = ("0" + (now.getMonth() + 1)).slice(-2);
					d = ("0" + now.getDate()).slice(-2);
					data[i].pair = data[i].pair.replace(/\-/g, "/");
					html +='<li class="refinelist">'+
						'<div class="refinelistcoontent">'+
						'<div class="refineli_date">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</div>'+
						'<div class="refineli_left">'+
							'<p class = "refinelipair">'+data[i].pair+'</p>'+
							'<span class="refinelitype">'+data[i].type+'</span>'+
						'</div>'+
						'<div class="refineli_right">'+
							'<span class="righttop">'+data[i].price+'</span>'+
							'<p>'+data[i].amount+'</p>'+
						'</div>'+
						'<div class="refinearrow"></div>'+
						'<span class="clicknum" style="display: none;">'+data[i].orderId+'</span>'+
						'</div>'+
						'<div class="refineulcancel"><div class="refineulcancelbtn">'+get_lan("CANCEL")+'</div></div>'
					'</li>'
					}
					$('#refineul').html(html)
				}
				
				$("#refineul .refinearrow").each(function(){
					$(this).on('tap',function(){ 
						var $pth = $(this).parent(".refinelistcoontent").siblings(".refineulcancel"); 
						if($(this).hasClass("refinearrowactive")){
							$(this).addClass("refinearrow")
							$(this).removeClass("refinearrowactive") 
							$pth.fadeOut();
							$pth.fadeOut(); 
						}else if($(this).hasClass("refinearrow")){ 
							$(this).removeClass("refinearrow");
							$(this).addClass("refinearrowactive");
							$pth.fadeIn();
							$pth.fadeIn(); 
						} 
					})
				})
			}
		});
	})
	//委托历史重置
	var pagenumber = 1;
	var pages = 0;
	$('#refineboxtworeset').on('tap',function(){
// 		if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('en') == null){
// 			$('#refineboxcoinone').html('ALL');
// 			$('#refineboxtwocointwo').html('ALL');
// 			$("#refineboxtwotype").html('ALL');
// 		}else{
// 			$('#refineboxcoinone').html('ALL');
// 			$('#refineboxtwocointwo').html('ALL');
// 			$("#refineboxtwotype").html('ALL');
// 		}
		pagenumber = 1;
		pages = 0;
		$('#historypageTotal').html('0');
		$('#historypageindex').html('0');
		$('#refineboxtwocoinone').html(get_lan('all'));
		$('#refineboxtwocointwo').html(get_lan('all'));
		$("#refineboxtwotype").html(get_lan('all'));
		$('#historyone').html('-');
		$('#historytwo').html('-');
		$('#historytype').html('- -');
		//document.getElementById('refineboxtwo').style.left = '100%';
		$('#refineultwo').html('<li class="refinelist"><div class="nomore">'+get_lan("Pleaseselectapair")+'</div></li>');
	})
	//委托历史订单筛选
	$('#refineboxtwosubmit').on('tap',function(){
		$("#historypageprevious").unbind('tap');
		$("#historypagenext").unbind('tap');
		$("#historypagehome").unbind('tap');
		$("#historypagetail").unbind('tap');
		var refineboxtwocoinone = $('#refineboxtwocoinone').html();
		var refineboxtwocointwo = $('#refineboxtwocointwo').html();
		var refineboxtwotype = $('#refineboxtwotype').html();
		if(refineboxtwotype == 'ALL' || refineboxtwotype == '所有'){
			refineboxtwotype = '';
		}else if(refineboxtwotype == '买' || refineboxtwotype == 'BUY'){
			refineboxtwotype = 'buy';
		}else if(refineboxtwotype == '卖' || refineboxtwotype == 'SELL'){
			refineboxtwotype = 'sell';
		}
		var refineboxtwosubmit = refineboxtwocoinone + '-' + refineboxtwocointwo;
		var refinehistoryname =  refineboxtwocoinone + '/' + refineboxtwocointwo;
		$.ajax({
		    url: 'https://rest.glenbit.com/order/dealt/' + refineboxtwosubmit,
		    type: 'POST',
		    dataType: 'json',
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
		    data: {
				type: refineboxtwotype,
				page: '1',
				limit: 10
		    },
			success:(function (data) {
				if(data.errors != undefined){
					mui.toast(get_lan('FAILURE'))
					return;
				}
				loginstatus(data)
				console.log(data);
				document.getElementById('refineboxtwo').style.left = '100%';
				$('#historyone').html(refineboxtwocoinone);
				$('#historytwo').html(refineboxtwocointwo);
				$('#historytype').html($('#refineboxtwotype').html());
				if(data.docs == ''){
					$('#historypageTotal').html('0');
					$('#historypageindex').html('0');
					$('#refineultwo').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>');
				}else{
					var html = '';
					for(var i = 0 ; i < data.docs.length ; i++){
					if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('lan') == null){
						if(data.docs[i].type == 'buy'){
							data.docs[i].type = '买入'
						}else if(data.docs[i].type == 'sell'){
							data.docs[i].type = '卖出'
						}
					}
					var now = new Date(parseInt(data.docs[i].createdAt));
					y = now.getFullYear();
					m = ("0" + (now.getMonth() + 1)).slice(-2);
					d = ("0" + now.getDate()).slice(-2);
					html +='<li class="refinelist">'+
						'<div class="refinelistcoontent">'+
						'<div class="refineli_date">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</div>'+
						'<div class="refineli_left">'+
							'<p class = "refinelipair">'+refinehistoryname+'</p>'+
							'<span class="refinelitype">'+data.docs[i].type+'</span>'+
						'</div>'+
						'<div class="refineli_right">'+
							'<span class="righttop">'+data.docs[i].price+'</span>'+
							'<p>'+data.docs[i].amount+'</p>'+
						'</div>'+
						'</div>'+
					'</li>'
					}
					$('#refineultwo').html(html);
					$('#historypageTotal').html(data.pages);
					pages = data.pages;
					$('#historypageindex').html('1');
					//下一页
					$('#historypagenext').on('tap',function(){
						if(pagenumber < pages){
							pagenumber++;
							pageajax(pagenumber)
						}
						event.stopPropagation();
					})
					//上一页
					$('#historypageprevious').on('tap',function(){
						if(pagenumber > 1){
							pagenumber--;
							pageajax(pagenumber)
						}
						event.stopPropagation();
					})
					//首页
					$('#historypagehome').on('tap',function(){
						if(pagenumber > 1){
							pagenumber = 1;
							pageajax(pagenumber)
						}
						event.stopPropagation();
					})
					//尾页
					$('#historypagetail').on('tap',function(){
						if(pagenumber < pages){
							pagenumber = pages;
							pageajax(pagenumber)
						}
						event.stopPropagation();
					})
					function pageajax(page){
						$.ajax({
						    url: 'https://rest.glenbit.com/order/dealt/' + refineboxtwosubmit,
						    type: 'POST',
						    dataType: 'json',
							xhrFields: {
								withCredentials: true
							},
							crossDomain: true,
						    data: {
								type: refineboxtwotype,
								page: page,
								limit: 10
						    },
							success:(function (data) {
								loginstatus(data)
								var html = '';
								for(var i = 0 ; i < data.docs.length ; i++){
								if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('lan') == null){
									if(data.docs[i].type == 'buy'){
										data.docs[i].type = '买入'
									}else if(data.docs[i].type == 'sell'){
										data.docs[i].type = '卖出'
									}
								}
								var now = new Date(parseInt(data.docs[i].createdAt));
								y = now.getFullYear();
								m = ("0" + (now.getMonth() + 1)).slice(-2);
								d = ("0" + now.getDate()).slice(-2);
								html +='<li class="refinelist">'+
									'<div class="refinelistcoontent">'+
									'<div class="refineli_date">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</div>'+
									'<div class="refineli_left">'+
										'<p class = "refinelipair">'+refinehistoryname+'</p>'+
										'<span class="refinelitype">'+data.docs[i].type+'</span>'+
									'</div>'+
									'<div class="refineli_right">'+
										'<span class="righttop">'+data.docs[i].price+'</span>'+
										'<p>'+data.docs[i].amount+'</p>'+
									'</div>'+
									'</div>'+
								'</li>'
								}
								$('#refineultwo').html(html);
								$('#historypageindex').html(pagenumber);
							})
						})
					}
				}
				
			}),
			error:function(){
				mui.toast(get_lan('FAILURE'))
			}
		});
		//分页选择
		//$('#')
		
	})
	
	//历史订单请求
	
	
	