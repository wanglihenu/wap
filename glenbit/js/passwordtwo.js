mui.init({
	 		preloadPages:[
		 		{
		 			'url':'my.html' 
		 		} 
	 		]
	 	})
		$("#Newloginpassword").attr("placeholder",get_lan("PASSWORD_PLZ"))
		$("#verificationcode").attr("placeholder",get_lan("CONFIRM_PASSWORD_PLZ"))
		//提交表单
		document.getElementById("gobackpages").addEventListener('tap',function(){
			mui.openWindow({
				url:'Resetloginpassword.html'
			});
		})
			   function render_target(extras){
				  // target.innerHTML = 'name的值为' + data.name
				  document.getElementsByClassName("buttonsubmit")[0].addEventListener('tap', function() {
				  	var sendData = {} 
				  	//获取新密码的值
				  	var new_login_password = document.getElementById("Newloginpassword").value;
				  	var verificationcode = document.getElementById("verificationcode").value;
				  	sendData["new_login_password"] = new_login_password;
				  	sendData["verificationcode"] = verificationcode;
				  	//校验输入框不为空
					
				  	if((new_login_password!='')){
				  		//对比输入两次的密码是否一致
				  	 	if(verificationcode != new_login_password ){
				  	 		mui.toast(get_lan("CONFIRM_PASSWORD_INVALID"))
				  	 	}else{
								//提交部分数据部分
								if(extras.version = false){
								   $.ajax('https://rest.glenbit.com/account/modify_password_phone',{
									data:{
										password: new_login_password,
										code: extras.name
									},
									dataType: 'json',
									xhrFields: {
										withCredentials: true
									},
									crossDomain: true,
									type:'post',//HTTP请求类型
									timeout:10000,//超时时间设置为10秒；
									success:function(data){
									},
									error:function(xhr,type,errorThrown){
										
									}
								   });
								}else if(extras.version = true){
									$.ajax('https://rest.glenbit.com/account/modify_password',{
									data:{
										password: new_login_password,
										code: extras.name
									},
									dataType: 'json',
									xhrFields: {
										withCredentials: true
									},
									crossDomain: true,
									type:'post',//HTTP请求类型
									timeout:30000,//超时时间设置为10秒；
									success:function(data){
										if(data.errors != undefined){
												mui.toast(data.errors[0].msg)
										}else{
											mui.toast(get_lan('SUCCESS'))
											mui.openWindow({
												url:'login.html'
											});
											localStorage.removeItem('username');
										}
									},
									error:function(xhr,type,errorThrown){
										
									}
									});
								}
						}
				  		
				  	}else{
				  		//输入框不符合要求的情况下进行处理 
						if(new_login_password == ''){
							mui.toast(get_lan("PASSWORD_PLZ"))
						}else if(new_login_password < 6){
							mui.toast(get_lan("PASSWORD_INVALID"))
						}
				  	} 
				  	//localStorage.setItem("type","loginpassword")
// 				  	mui.openWindow({ //目标页面
// 				  		url: 'index.html' 
// 				     })
				  })
			   }
			   // 同步调用
			   // 异步调用
			   mui.getExtras(function(extras){
				   render_target(extras);
			   });