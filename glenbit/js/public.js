mui.init({
	swipeBack: false,
	drag: false
});
document.getElementsByClassName('mui-inner-wrap')[0].addEventListener('drag', function(event) {
	event.stopPropagation();
});
 //侧滑容器父节点
var offCanvasWrapper = mui('#offCanvasWrapper');
 //主界面容器
var offCanvasInner = offCanvasWrapper[0].querySelector('.mui-inner-wrap');
 //菜单容器
var offCanvasSide = document.getElementById("offCanvasSide");
 //Android暂不支持整体移动动画
if (!mui.os.android) {
	//document.getElementById("move-togger").classList.remove('mui-hidden');
	var spans = document.querySelectorAll('.android-only');
	for (var i = 0, len = spans.length; i < len; i++) {
		spans[i].style.display = "none";
	}
}
 //移动效果是否为整体移动
var moveTogether = true;
offCanvasInner.insertBefore(offCanvasSide, offCanvasInner.firstElementChild);
document.getElementById('offCanvasHide').addEventListener('tap', function() {
	offCanvasWrapper.offCanvas('close');
});
 //侧滑容器的class列表，增加.mui-slide-in即可实现菜单移动、主界面不动的效果；
//var classList = offCanvasWrapper[0].classList;
 //主界面和侧滑菜单界面均支持区域滚动；
mui('#offCanvasSideScroll').scroll();
mui('#offCanvasContentScroll').scroll();
mui('#searchscroll').scroll();
mui('#selectscroll').scroll();
mui('#exchangescroll').scroll();
mui("#favoritescroll").scroll();
mui("#buyscroll").scroll();
mui("#sellscroll").scroll();
mui('#DepositContentscroll').scroll();
mui("#widthdrawContentscroll").scroll();
mui("#historyscroll").scroll();
mui("#openContentScroll").scroll();
mui("#historyContentScroll").scroll();
//监听浏览器关闭
//头部信息显示
$(function(){
	var userinfo = localStorage.getItem('username');
	if(userinfo == undefined || userinfo == null){
		$('.username').html(get_lan('Notloggedin'));
		document.getElementById('userinfo').addEventListener('tap',function(){
			mui.openWindow({
				url:'login.html'
			});
		});
	}else{
		$('.username').html("");
		document.getElementById('userinfo').addEventListener('tap',function(){
			mui.openWindow({
				url:'my.html'
			});
		});
	}
})
//验证登录状态
function loginstatus(data){
	if(data.errors != undefined){
		if(data.errors[0].code == 4000){
			mui.openWindow({
				url:'login.html'
			});
			localStorage.removeItem('username');
		}
	}
	if(localStorage.getItem('username') == null || userinfo == undefined){
		mui.openWindow({
			url:'login.html'
		});
		localStorage.removeItem('username');
	}
}
//多语言相关
	$("#gochinese").on('tap',function(){
		localStorage.setItem("lan","cn")
		window.location.href = location.href+'?time='+((new Date()).getTime());
	});
	$("#goenglish").on("tap",function(){
		localStorage.setItem("lan","en")
		window.location.href = location.href+'?time='+((new Date()).getTime());
	})
	$('[set-lan]').each(function(){
    var me = $(this);
    var a = me.attr('set-lan').split(':');
    var p = a[0];   //文字放置位置
    var m = a[1];   //文字的标识

    //用户选择语言后保存在cookie中，这里读取cookie中的语言版本
    var lan = localStorage.getItem("lan");

    //选取语言文字
    switch(lan){
        case 'cn':
            var t = cn[m];  //这里cn[m]中的cn是上面定义的json字符串的变量名，m是json中的键，用此方式读取到json中的值
            break;
        case 'en':
            var t = en[m];
            break;
        default:
            var t = cn[m];
    }

    //如果所选语言的json中没有此内容就选取其他语言显示
    if(t==undefined) t = cn[m];
    if(t==undefined) t = en[m];

    if(t==undefined) return true;   //如果还是没有就跳出

    //文字放置位置有（html,val等，可以自己添加）
    switch(p){
        case 'html':
            me.html(t);
            break;
        case 'val':
        case 'value':
            me.val(t);
            break;
        default:
            me.html(t);
    }

});
function get_lan(m)
{
    //获取文字
    var lan = localStorage.getItem("lan");    //语言版本
    //选取语言文字
    switch(lan){
        case 'cn':
            var t = cn[m];
            break;
        case 'en':
            var t = en[m];
            break;
        default:
            var t = cn[m];
    }

    //如果所选语言的json中没有此内容就选取其他语言显示
    if(t==undefined) t = cn[m];
    if(t==undefined) t = en[m];

    if(t==undefined) t = m; //如果还是没有就返回他的标识

    return t;
}
//
if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('lan') == null){
	$('.cnSelection').css('display','block');
	$('.enSelection').css('display','none');
}else{
	$('.cnSelection').css('display','none');
	$('.enSelection').css('display','block');
}
///导航链接
document.getElementById('logo').addEventListener('tap',function(){
	mui.openWindow({
		url:'index.html'
	});
});
document.getElementById('gohome').addEventListener('tap',function(){
	mui.openWindow({
		url:'index.html'
	});
});
document.getElementById('goFinancing').addEventListener('tap',function(){
	alert(get_lan('Comingsoon'))
});
document.getElementById('goexchange').addEventListener('tap',function(){
	mui.openWindow({
		url:'charting_library-master/TradingCenter.html',
		extras: {
			currency:'BTC/USDT',
		}
	});
});
document.getElementById('goInvitationrebate').addEventListener('tap',function(){
	$.ajax({
		url: 'https://rest.glenbit.com/account/profile',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success: function (data) {
			if(data.errors != undefined){
				if(data.errors[0].code == 4000){
					mui.openWindow({
						url:'login.html'
					});
				}
			}else{
				mui.openWindow({
				url:'Invitationrebate.html',
				
				})
			}
		},
	})
});
document.getElementById('goassetRortfolio').addEventListener('tap',function(){
	$.ajax({
		url: 'https://rest.glenbit.com/account/profile',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success: function (data) {
			if(data.errors != undefined){
				if(data.errors[0].code == 4000){
					mui.openWindow({
						url:'login.html'
					});
				}
			}else{
				mui.openWindow({
				url:'AssetDepositWithdrawal.html',
				
				})
			}
		},
	})
});

document.getElementById('gooverview').addEventListener('tap',function(){
	$.ajax({
		url: 'https://rest.glenbit.com/account/profile',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success: function (data) {
			if(data.errors != undefined){
				if(data.errors[0].code == 4000){
					mui.openWindow({
						url:'login.html'
					});
				}
			}else{
				mui.openWindow({
				url:'my.html',
				})
			}
		},
	})
});
document.getElementById('goOpenOrders').addEventListener('tap',function(){
	$.ajax({
		url: 'https://rest.glenbit.com/account/profile',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success: function (data) {
			if(data.errors != undefined){
				if(data.errors[0].code == 4000){
					mui.openWindow({
						url:'login.html'
					});
				}
			}else{
				mui.openWindow({
				url:'ordersandtrade.html',
				extras: {
					type:'open',
				},
				})
			}
		},
	})
});
document.getElementById('goOpenOrdershistory').addEventListener('tap',function(){
	$.ajax({
		url: 'https://rest.glenbit.com/account/profile',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success: function (data) {
			if(data.errors != undefined){
				if(data.errors[0].code == 4000){
					mui.openWindow({
						url:'login.html'
					});
				}
			}else{
				mui.openWindow({
				url:'ordersandtrade.html',
				extras: {
					type:'history',
				},
				})
			}
		},
	})
});
document.getElementById('mycentertab').addEventListener('tap',function(){
	$(".mycenterlist").slideToggle(500)
	$(".mycenterArrow").toggle();
	$(".mycenterArrowtwo").toggle();
			
});
document.getElementById('languagetab').addEventListener('tap',function(){
	$(".langagetab").slideToggle(500)
	$(".languageArrow").toggle();
	$(".languageArrowtwo").toggle();
});

document.getElementById('footer0').addEventListener('tap',function(){
	mui.openWindow({
		url:'https://www.facebook.com/GlenBitOfficial'
	});
});
document.getElementById('footer1').addEventListener('tap',function(){
	mui.openWindow({
		url:'https://t.me/glenbitofficial'
	});
});
document.getElementById('footer2').addEventListener('tap',function(){
	mui.openWindow({
		url:'https://twitter.com/glenbitofficial'
	});
});
document.getElementById('footer3').addEventListener('tap',function(){
	mui.openWindow({
		url:'https://www.reddit.com/user/GlenBitOfficial'
	});
});
document.getElementById('footer4').addEventListener('tap',function(){
	mui.openWindow({
		url:'https://medium.com/@GlenBitOfficial'
	});
});
document.getElementById('goaboutus').addEventListener('tap',function(){
	if(localStorage.getItem('lan') == undefined || localStorage.getItem('lan') == null || localStorage.getItem('lan') == 'cn'){
		mui.openWindow({
			url:'https://support.glenbit.com/article/8',
		});
	}else{
		mui.openWindow({
			url:'https://support.glenbit.com/article/8?lang=en',
		});
	}
});
document.getElementById('goservice').addEventListener('tap',function(){
	if(localStorage.getItem('lan') == undefined || localStorage.getItem('lan') == null || localStorage.getItem('lan') == 'cn'){
		mui.openWindow({
			url:'https://support.glenbit.com/article/7',
		});
	}else{
		mui.openWindow({
			url:'https://support.glenbit.com/article/7?lang=en',
		});
	}
});
document.getElementById('goPrivacy').addEventListener('tap',function(){
	if(localStorage.getItem('lan') == undefined || localStorage.getItem('lan') == null || localStorage.getItem('lan') == 'cn'){
		mui.openWindow({
			url:'https://support.glenbit.com/article/6',
		});
	}else{
		mui.openWindow({
			url:'https://support.glenbit.com/article/6?lang=en',
		});
	}
});
document.getElementById('gofees').addEventListener('tap',function(){
	if(localStorage.getItem('lan') == undefined || localStorage.getItem('lan') == null || localStorage.getItem('lan') == 'cn'){
		mui.openWindow({
			url:'https://support.glenbit.com/article/10',
		});
	}else{
		mui.openWindow({
			url:'https://support.glenbit.com/article/10?lang=en',
		});
	}
});