mui.init({
	swipeBack: false,
	drag: false
});
$('body').on('tap',function(){
	$('input').blur();
})
$("input").blur(function(){
	$("input").css("border","1px solid #353B41");
  
});
//$("input").focus(
$('.buypriceinput').focus( function (){
	if($(this).val() == ''){
		$(this).css("border","2px solid rgba(240,74,93,0.2)");
	}else{
		$(this).css("border","2px solid rgba(0,172,220,0.2)");
	}	
});
$('.sellpriceinput').focus( function (){
	if($(this).val() == ''){
		$(this).css("border","2px solid rgba(240,74,93,0.2)");
	}else{
		$(this).css("border","2px solid rgba(0,172,220,0.2)");
	}	
});
$('.buynumberinput').focus( function (){
	if(buyinputcount == 0){
		$(this).css("border","2px solid rgba(0,172,220,0.2)");
	}else{
		if($(this).val() == ''){
			$(this).css("border","2px solid rgba(240,74,93,0.2)");
		}else{
			$(this).css("border","2px solid rgba(0,172,220,0.2)");
		}
	}	
});
$('.sellnumberinput').focus( function (){
	if(sellinputcount == 0){
		$(this).css("border","2px solid rgba(0,172,220,0.2)");
	}else{
		if($(this).val() == ''){
			$(this).css("border","2px solid rgba(240,74,93,0.2)");
		}else{
			$(this).css("border","2px solid rgba(0,172,220,0.2)");
		}
	}	
});
$('.gologin').on('tap',function(){
	mui.openWindow({
		url:'login.html'
	})
});
$('.gosign').on('tap',function(){
	mui.openWindow({
		url:'signup.html'
	})
});
mui("#offCanvasContentScroll").scroll();
mui("#LatestdeallistScroll").scroll();
mui("#EntrustulScroll").scroll();
mui("#myDealulScroll").scroll();
mui("#MarketchoiceScroll").scroll();

var bodyheight = document.documentElement.clientHeight;
var tradingheader = document.getElementsByClassName("tradingheader")[0];
var tradingheaderheight = tradingheader.clientHeight||tradingheader.offsetHeight;
var tradingscroll = bodyheight - tradingheaderheight;
document.getElementById('offCanvasContentScroll').style.height = tradingscroll + 'px';
//委托成交切换
$('#Mycommissiontabone').on('tap',function(){
	$('.myEntrust').css('display','block');
	$('.myDeal').css('display','none');
	$(this).css({'background':'#353B41','color':'#fff'});
	$('#Mycommissiontabtwo').css({'background':'none','color':'#93989d'});
})
$('#Mycommissiontabtwo').on('tap',function(){
	$('.myEntrust').css('display','none');
	$('.myDeal').css('display','block');
	$(this).css({'background':'#353B41','color':'#fff'});
	$('#Mycommissiontabone').css({'background':'none','color':'#93989d'});
})
//end

//end
//市场切换
//多语言
	$('[set-lan]').each(function(){
    var me = $(this);
    var a = me.attr('set-lan').split(':');
    var p = a[0];   //文字放置位置
    var m = a[1];   //文字的标识

    //用户选择语言后保存在cookie中，这里读取cookie中的语言版本
    var lan = localStorage.getItem("lan");

    //选取语言文字
    switch(lan){
        case 'cn':
            var t = cn[m];  //这里cn[m]中的cn是上面定义的json字符串的变量名，m是json中的键，用此方式读取到json中的值
            break;
        case 'en':
            var t = en[m];
            break;
        default:
            var t = cn[m];
    }

    //如果所选语言的json中没有此内容就选取其他语言显示
    if(t==undefined) t = cn[m];
    if(t==undefined) t = en[m];

    if(t==undefined) return true;   //如果还是没有就跳出

    //文字放置位置有（html,val等，可以自己添加）
    switch(p){
        case 'html':
            me.html(t);
            break;
        case 'val':
        case 'value':
            me.val(t);
            break;
        default:
            me.html(t);
    }

});
function get_lan(m)
{
    //获取文字
    var lan = localStorage.getItem("lan");    //语言版本
    //选取语言文字
    switch(lan){
        case 'cn':
            var t = cn[m];
            break;
        case 'en':
            var t = en[m];
            break;
        default:
            var t = cn[m];
    }

    //如果所选语言的json中没有此内容就选取其他语言显示
    if(t==undefined) t = cn[m];
    if(t==undefined) t = en[m];

    if(t==undefined) t = m; //如果还是没有就返回他的标识

    return t;
}
//市场选择
$('.tradingtext').on('tap',function(){
	$('.Marketchoice').slideToggle(200);
	$('.tradingciontab').slideToggle(0);
	$('.tradingciontabtwo').slideToggle(0);
	
})
$('.buypriceinput').attr('placeholder',get_lan("BUY_PRICE"))
$('.buynumberinput').attr('placeholder',get_lan("BUY_AMOUNT"))
$('.sellpriceinput').attr('placeholder',get_lan("SELL_PRICE"))
$('.sellnumberinput').attr('placeholder',get_lan("SELL_AMOUNT"))
////滚动条
var Selectionofcontent = 0;
$('.Selectionofcontent').each(function(){
	$(this).on('tap',function(){
		if($(this).css("color") == 'rgb(255, 255, 255)'){
			Selectionofcontent = $(this).find('span').html();
			$(this).css({'color':'#f0d22e','border':'1px solid #f0d22e'})
			$(this).siblings('.Selectionofcontent').css({'color':'#fff','border':'1px solid #353b41'})
		}else{
			Selectionofcontent = 0
			$(this).css({'color':'#fff','border':'1px solid #353b41'})
			$(this).siblings('.Selectionofcontent').css({'color':'#fff','border':'1px solid #353b41'})
		}
		console.log(Selectionofcontent);
		var buyPercentage = Selectionofcontent / 100;
		var buyneedcurrencynumber = $('.buyneedcurrencynumber').html();
		var buypriceinput = $('.buypriceinput').val();
		console.log((Number(buyneedcurrencynumber) / Number(buypriceinput) * buyPercentage).toFixed(6))
		$('.buynumberinput').val((Number(buyneedcurrencynumber) / Number(buypriceinput) * Number(buyPercentage)).toFixed(6));
		$('.buyTotalprice').html((Number(buypriceinput) * (Number(buyneedcurrencynumber) / Number(buypriceinput) * Number(buyPercentage))).toFixed(8));
	})
})
$('.sellSelectionofcontent').on('tap',function(){
	if($(this).css("color") == 'rgb(255, 255, 255)'){
		Selectionofcontent = $(this).find('span').html();
		$(this).css({'color':'#f0d22e','border':'1px solid #f0d22e'})
		$(this).siblings('.sellSelectionofcontent').css({'color':'#fff','border':'1px solid #353b41'})
	}else{
		Selectionofcontent = 0
		$(this).css({'color':'#fff','border':'1px solid #353b41'})
		$(this).siblings('.sellSelectionofcontent').css({'color':'#fff','border':'1px solid #353b41'})
	}
	console.log(Selectionofcontent);
	var changevalue = $('.sellpriceinput').val();
	var sellneedcurrencynumber = $('.sellneedcurrencynumber').html();
	var buyPercentage = Selectionofcontent / 100;
	$('.sellnumberinput').val((Number(sellneedcurrencynumber) * Number(buyPercentage)).toFixed(6));
	$('.sellTotalprice').html((Number(changevalue) * (Number(sellneedcurrencynumber) * Number(buyPercentage))).toFixed(8))
})
//列表选中


//全局变量
var marketscion = 'GL';
var fromcion = '';
var tocion = '';
var socket = 0;
var minVolume = 0;
var wsexchange = '';
var Transactiontype = '';
var buyhtml = '';
var sellhtml = '';
var buyinputcount = 0;
var sellinputcount = 0;
//加载取值
mui.getExtras(function(extras){
	var newwxtras = '';
		if(extras.currency == undefined){
			extras.currency = 'BTC/USDT';
		}
		if(extras.currency.indexOf('%2F') != -1){
			newwxtras = extras.currency.split('%2F');
			
		}else if(extras.currency.indexOf('-') != -1){
			newwxtras = extras.currency.split('-');
		}
	if(newwxtras == ''){
		   mui.openWindow({
			   url:'index.html'
		   })
	}
   Transactiontype = extras.Transactiontype;
   wsexchange = newwxtras[0] +'-'+ newwxtras[1];
   fromcion = newwxtras[0];
   tocion = newwxtras[1];
   $('.tradingtext span').html(newwxtras[0] +'/'+ newwxtras[1]);
   $('#tradingback').on('tap',function(){
		mui.openWindow({
			url:"charting_library-master/TradingCenter.html",
			extras: {
				currency : newwxtras[0] +'/'+ newwxtras[1],
			}
		})
   })
   $('.sellcion').html(newwxtras[0])
   $('.buycion').html(newwxtras[1])
   if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('lan') == null){
	   
	   $('.tradingpricetitle').html(get_lan('PRICE') +'('+ tocion +')')
	   $('.tradingamounttitle').html(get_lan('Amount') +'('+ fromcion +')')
   }else if(localStorage.getItem('lan') == 'en'){
	   $('.tradingpricetitle').html(get_lan('PRICE'))
	   $('.tradingamounttitle').html(get_lan('Amount'))
   }
   
});
//默认交易类型

if(Transactiontype == 'buy' || Transactiontype == undefined){
	$('#transactiontabone').css({'background':'#353B41','color':'#fff'})
	$('#transactiontabtwo').css({'background':'none','color':'#93989d'})
	$('#buycontent').css('display','block');
	$('#sellcontent').css('display','none');
}else if(Transactiontype == 'sell'){
	$('#transactiontabone').css({'background':'none','color':'#93989d'})
	$('#transactiontabtwo').css({'background':'#353B41','color':'#fff'})
	$('#buycontent').css('display','none');
	$('#sellcontent').css('display','block');
}
//买卖切换

$('#transactiontabone').on('tap',function(){
	Transactiontype = 'buy'
	$('#Entrustul').html(buyhtml);
	$('#buycontent').css('display','block');
	$('#sellcontent').css('display','none');
	$(this).css({'background':'#353B41','color':'#fff'});
	$('#transactiontabtwo').css({'background':'none','color':'#93989d'});
	$("#Entrustul .Entrustulcontent").each(function(){
		$(this).on('tap',function(){
			$(this).siblings('.Entrustulcancel').slideToggle(200);
			$(this).toggleClass('Entrustulcontentbg')
		})
	})
})
$('#transactiontabtwo').on('tap',function(){
	Transactiontype = 'sell'
	$('#Entrustul').html(sellhtml);
	$('#buycontent').css('display','none');
	$('#sellcontent').css('display','block');
	$(this).css({'background':'#353B41','color':'#fff'});
	$('#transactiontabone').css({'background':'none','color':'#93989d'});
	$("#Entrustul .Entrustulcontent").each(function(){
		$(this).on('tap',function(){
			$(this).siblings('.Entrustulcancel').slideToggle(200);
			$(this).toggleClass('Entrustulcontentbg')
		})
	})
})
//撤单显示切换

//购买双向绑定
function buyinputprice(){
	var buyinputprice = document.getElementsByClassName('buypriceinput')[0].value;
	var buyinputnum = document.getElementsByClassName('buynumberinput')[0].value;
	$('.buyTotalprice').html((Number(buyinputprice) * Number(buyinputnum)).toFixed(8))
	$('.buymaxnum').html(parseInt(Number(buytocion) / Number(buyinputprice)));
	if(buyinputprice == '' || isNaN == true){
		$('.buypriceinput').css('border','2px solid rgba(240,74,93,0.2)');
	}else{
		$('.buypriceinput').css('border','2px solid rgba(0,172,220,0.2)');
	}
}
function buyinputnum(){
	var buyinputprice = document.getElementsByClassName('buypriceinput')[0].value;
	var buyinputnum = document.getElementsByClassName('buynumberinput')[0].value;
	$('.buyTotalprice').html((Number(buyinputprice) * Number(buyinputnum)).toFixed(8))
	if(buyinputnum == '' || isNaN == true){
		$('.buynumberinput').css('border','2px solid rgba(240,74,93,0.2)');
	}else{
		$('.buynumberinput').css('border','2px solid rgba(0,172,220,0.2)');
	}
	buyinputcount = 1;
}
//出售双向绑定
function sellinputprice(){
	var sellinputprice = document.getElementsByClassName('sellpriceinput')[0].value;
	var sellinputnum = document.getElementsByClassName('sellnumberinput')[0].value;
	$('.sellTotalprice').html((Number(sellinputprice) * Number(sellinputnum)).toFixed(8));
	if(sellinputprice == '' || isNaN == true){
		$('.sellpriceinput').css('border','2px solid rgba(240,74,93,0.2)');
	}else{
		$('.sellpriceinput').css('border','2px solid rgba(0,172,220,0.2)');
	}
}
function sellinputnum(){
	var sellinputprice = document.getElementsByClassName('sellpriceinput')[0].value;
	var sellinputnum = document.getElementsByClassName('sellnumberinput')[0].value;
	$('.sellTotalprice').html((Number(sellinputprice) * Number(sellinputnum)).toFixed(8))
	if(sellinputnum == '' || isNaN == true){
		$('.sellnumberinput').css('border','2px solid rgba(240,74,93,0.2)');
	}else{
		$('.sellnumberinput').css('border','2px solid rgba(0,172,220,0.2)');
	}
	sellinputcount = 1;
}
//购买出售函数
function buyorsell(type,price,amount){
	
	$.ajax({
		url:'https://rest.glenbit.com/order/make',
		type:'POST',
		dataType:'json',
		xhrFields: {
			withCredentials: true
		},
		data: {
			pair: fromcion+'-'+tocion,
			type: type,
			price: price,
			amount: amount
		},
		crossDomain: true,
		success:function(data){
			var newpricetwo = $('.newprice').html();
			if(data.errors == undefined){
				if(type == 'buy'){
					
					$.ajax({
						url:"https://rest.glenbit.com/order/open/" + wsexchange,
						type: 'POST',
						dataType: 'json',
						xhrFields: {
							withCredentials: true
						},
						crossDomain: true,
						success:function(data){
							var html = '';
							if(data.errors != undefined){
								//mui.toast(data.errors[0].msg)
								if(data.errors[0].code == 4000){
									$('.pleaselogin').css('display','block')
								}
								return;
							}else{
								$('.pleaselogin').css('display','none')
							}
							function sortId(a,b){  
							   return b.createdAt-a.createdAt  
							}
							data.sort(sortId);
							if(data == ''){
								//$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
							}else{
								for(var i = 0 ; i < data.length ; i++){
									if(data[i].type == 'buy'){
										html +='<li>'+
													'<div class="Entrustulcontent">'+
														'<div>'+data[i].price.toFixed(6)+'</div>'+
														'<div>'+data[i].amount.toFixed(4)+'</div>'+	
													'</div>'+
													'<span class="Entrustid" style="display: none;">'+data[i].orderId+'</span>'+
													'<span class="Entrusttype" style="display: none;">'+data[i].type+'</span>'+
													'<div class="Entrustulcancel">'+get_lan("CANCEL")+'</div>'+
												'</li>'
										}
									}
									buyhtml = html;
									$('#Entrustul').html(html);
// 										if(Transactiontype == 'buy' || Transactiontype == undefined){
// 											$('#Entrustul').html(buyhtml);
// 										}else if(Transactiontype == 'sell'){
// 											//$('#Entrustul').html(sellhtml);
// 										}
									}
								
								$("#Entrustul .Entrustulcontent").each(function(){
									$(this).on('tap',function(){
										$(this).siblings('.Entrustulcancel').slideToggle(200);
										$(this).toggleClass('Entrustulcontentbg')
									})
								})
								
							}
					});
					
					if(price < (newpricetwo)){
						
						mui.toast(get_lan("restingorder"))
					}else{
						mui.toast(get_lan("Purchasesuccess"))
					}
					//mui.toast(get_lan("Purchasesuccess"))
					setTimeout(function() {
						mui('.buytipsbtn').button('reset');
					}.bind(this), 1500);
				}else if(type == 'sell'){
					$.ajax({
						url:"https://rest.glenbit.com/order/open/" + wsexchange,
						type: 'POST',
						dataType: 'json',
						xhrFields: {
							withCredentials: true
						},
						crossDomain: true,
						success:function(data){
							var html = '';
							if(data.errors != undefined){
								//mui.toast(data.errors[0].msg)
								if(data.errors[0].code == 4000){
									$('.pleaselogin').css('display','block')
								}
								return;
							}else{
								$('.pleaselogin').css('display','none')
							}
							function sortId(a,b){  
							   return b.createdAt-a.createdAt  
							}
							data.sort(sortId);
							if(data == ''){
								//$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
							}else{
								for(var i = 0 ; i < data.length ; i++){
										if(data[i].type == 'sell'){
											html +='<li>'+
													'<div class="Entrustulcontent">'+
														'<div>'+data[i].price.toFixed(6)+'</div>'+
														'<div>'+data[i].amount.toFixed(4)+'</div>'+	
													'</div>'+
													'<span class="Entrustid" style="display: none;">'+data[i].orderId+'</span>'+
													'<span class="Entrusttype" style="display: none;">'+data[i].type+'</span>'+
													'<div class="Entrustulcancel">'+get_lan("CANCEL")+'</div>'+
												'</li>'
										}
									}
									sellhtml = html;
									$('#Entrustul').html(html);
// 										if(Transactiontype == 'buy' || Transactiontype == undefined){
// 											$('#Entrustul').html(buyhtml);
// 										}else if(Transactiontype == 'sell'){
// 											//$('#Entrustul').html(sellhtml);
// 										}
									}
								
								$("#Entrustul .Entrustulcontent").each(function(){
									$(this).on('tap',function(){
										$(this).siblings('.Entrustulcancel').slideToggle(200);
										$(this).toggleClass('Entrustulcontentbg')
									})
								})
								
							}
					});
					if(price > (newpricetwo)){
						mui.toast(get_lan("restingorder"))
					}else{
						mui.toast(get_lan("Salesuccess"))
					}
					setTimeout(function() {
						mui('.selltipsbtn').button('reset');
					}.bind(this), 1500);
				}
				
			}else {
				if(type == 'buy'){
					mui.toast(get_lan("Purchasefailure"))
					setTimeout(function() {
						mui('.buytipsbtn').button('reset');
					}.bind(this), 1500);
				}else if(type == 'sell'){
					mui.toast(get_lan("Salefailure"))
					setTimeout(function() {
						mui('.selltipsbtn').button('reset');
					}.bind(this), 1500);
				}
			}
		},
	})
}
//弹窗购买出售
function buyorselltwo(type,price,amount){
	
	$.ajax({
		url:'https://rest.glenbit.com/order/make',
		type:'POST',
		dataType:'json',
		xhrFields: {
			withCredentials: true
		},
		data: {
			pair: fromcion+'-'+tocion,
			type: type,
			price: price,
			amount: amount
		},
		crossDomain: true,
		success:function(data){
			if(data.errors == undefined){
				if(type == 'buy'){
					
					mui.toast(get_lan("Purchasesuccess"))
				}else if(type == 'sell'){
					if(buyprice < (newprice * 1.1)){
						mui.toast(get_lan("restingorder"))
					}
					mui.toast(get_lan("Salesuccess"))
				}
				
			}else {
				if(type == 'buy'){
					mui.toast(get_lan("Purchasefailure"))
				}else if(type == 'sell'){
					mui.toast(get_lan("Salefailure"))
				}
			}
		},
	})
}
//购买出售
// $('.buytipsbtn').on('tap',function(){
// 	buyinputcount = 1;
// 	var newprice = $('.newprice').html(); 
// 	var buyprice = $('.buypriceinput').val();
// 	var buynumber = $('.buynumberinput').val();
// 	//console.log(buyprice)
// 	if(buyprice <= 0){
// 		mui.toast(get_lan("PriceInputError"))
// 	}else if(buynumber <= 0){
// 		mui.toast(get_lan("Errorinquantityinput"))
// 	}else if(buynumber >= minVolume){
// 		if(buyprice >= (newprice * 1.1)){
// 			$('#buyReconfirmbox').fadeIn();
// 		}else{
// 			buyorsell('buy',buyprice,buynumber);
// 		}
// 		
// 	}else{
// 		mui.toast(get_lan("Notlessthan") + minVolume)
// 	}	
// })
$('.buytipsbtn').attr('data-loading-text',get_lan('BUY'))
$('.selltipsbtn').attr('data-loading-text',get_lan('SELL'))
	mui(document.body).on('tap', '.buytipsbtn', function(e) {
		mui(this).button('loading');
		buyinputcount = 1;
		var newprice = $('.newprice').html(); 
		var buyprice = $('.buypriceinput').val();
		var buynumber = $('.buynumberinput').val();
		//console.log(buyprice)
		if(buyprice <= 0){
			mui.toast(get_lan("PriceInputError"))
			setTimeout(function() {
				mui(this).button('reset');
			}.bind(this), 1000);
		}else if(buynumber <= 0){
			mui.toast(get_lan("MinBuyAmount") + minVolume)
			setTimeout(function() {
				mui(this).button('reset');
			}.bind(this), 1000);
		}else if(buynumber >= minVolume){
			if(buyprice >= (newprice * 1.1)){
				$('#buyReconfirmbox').fadeIn();
				setTimeout(function() {
					mui(this).button('reset');
				}.bind(this), 1000);
			}else{
				buyorsell('buy',buyprice,buynumber);
			}
			
		}else{
			mui.toast(get_lan("MinBuyAmount") + minVolume)
			setTimeout(function() {
				mui(this).button('reset');
			}.bind(this), 1000);
		}
	});
mui(document.body).on('tap', '.selltipsbtn', function(e) {
	sellinputcount = 1;
	mui(this).button('loading');
	var newprice = $('.newprice').html();
	var sellprice = $('.sellpriceinput').val();
	var sellnumber = $('.sellnumberinput').val();
	//console.log(buyprice)
	if(sellprice <= 0){
		mui.toast(get_lan("PriceInputError"))
		setTimeout(function() {
			mui(this).button('reset');
		}.bind(this), 1000);
	}else if(sellnumber <= 0){
		mui.toast(get_lan("MinSellAmount") + minVolume)
		setTimeout(function() {
			mui(this).button('reset');
		}.bind(this), 1000);
	}
	else if(sellnumber >= minVolume){
		if(sellprice <= (newprice * 0.9)){
			$('#sellReconfirmbox').fadeIn();
			setTimeout(function() {
				mui(this).button('reset');
			}.bind(this), 1000);
		}else{
			buyorsell('sell',sellprice,sellnumber);
		}
		
	}else{
		mui.toast(get_lan("MinSellAmount") + minVolume)
		setTimeout(function() {
			mui(this).button('reset');
		}.bind(this), 1000);
	}
})
//警告弹窗
$('.Reconfirmcancel').on('tap',function(){
	$('.Reconfirmbox').fadeOut();
});
$('#buyReconfirmsubmit').on('tap',function(){
	var buyprice = $('.buypriceinput').val();
	var buynumber = $('.buynumberinput').val();
	//console.log(buyprice)
	buyorselltwo('buy',buyprice,buynumber);
	$('.Reconfirmbox').fadeOut();	

})
$('#sellReconfirmsubmit').on('tap',function(){
	var sellprice = $('.sellpriceinput').val();
	var sellnumber = $('.sellnumberinput').val();
	//console.log(buyprice)
	buyorselltwo('sell',sellprice,sellnumber);
	$('#sellReconfirmbox').fadeOut();
})
//列表控制输入框

$('body').on('tap','.controlinput li',function(){
	$('.controlinput li').css('background','none');
	$(this).css('background','#263743');
	
	$('.inputprice').val($(this).find('.controlinputprice').html())
})
function controlinputtap(){
	
}
//获取个人货币信息
$.ajax({
	url:'https://rest.glenbit.com/wallet/balance',
	type:'POST',
	dataType:'json',
	xhrFields: {
		withCredentials: true
	},
	crossDomain: true,
	success:function(data){
		if(data.errors != undefined){
			//mui.toast(data.errors[0].msg)
			if(data.errors[0].code == 4000){
				$('.pleaselogin').css('display','block')
			}
			return;
		}else{
			$('.pleaselogin').css('display','none')
		}
		if(data == ''){
			$('.sellneedcurrencynumber').html('0.0000');
			$('.buyneedcurrencynumber').html('0.0000');
		}
		for (var i = 0 ; i < data.length ; i++){
			
			if(fromcion == data[i].coin){
				selltocion = data[i].free;
				$('.sellneedcurrencynumber').html(data[i].free.toFixed(4));			
			}
			if(tocion == data[i].coin){
				buytocion = data[i].free;
				$('.buyneedcurrencynumber').html(data[i].free.toFixed(4));
			}
		}	
	},
})
//获取排序信息
var sortdata = [];
$.ajax({
	url:'https://rest.glenbit.com/coins',
	type:'GET',
	dataType:'json',
	xhrFields: {
		withCredentials: true
	},
	crossDomain: true,
	success:function(data){
		 function sortId(a,b){  
		   return a.sortOrder-b.sortOrder  
		}
		data.sort(sortId);
		sortdata = data;
		$.ajax({
			url:'https://rest.glenbit.com/markets',
			type:'get',
			dataType:'json',
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			success:function(data){
				var html = '';
				var newshichang = [];
				for(var i = 0 ;i < data.length; i++){
					if(fromcion == data[i].targetCoin && tocion == data[i].marketCoin){
						$('.tradingfees').html(data[i].fee * 100 +'%');
						minVolume = data[i].minVolume;
					}
					let arr= [];
					arr.push(data[i].targetCoin,data[i].marketCoin);
					for(var k=0;k<arr.length;k++){
						for(var j = 0;j<sortdata.length;j++){
							if(arr[k] === sortdata[j]._id){
								arr[k] = sortdata[j].sortOrder
							}else{
								arr[k] = arr[k]
							}
						}
					}
					data[i].sortOrder = arr.reverse().join('');
 					newshichang.push(data[i])
				}
				function compare(property){
					return function(a,b){
						var value1 = a[property];
						var value2 = b[property];
						return value1 - value2;
					}
				}
				newshichang.sort(compare('sortOrder'))
				for(var t = 0 ; t < newshichang.length; t++){
					html+='<li>'+newshichang[t].targetCoin+'/'+newshichang[t].marketCoin+'</li>'
				}
				$('.Marketchoiceul').html(html);
				$('.Marketchoiceul li').each(function(){
					$(this).on('tap',function(){
						var taphtml = $(this).html();
						$('.tradingtext span').html(taphtml)
						mui.openWindow({
							url:"trading.html",
							extras: {
								currency : taphtml,
							}
						})
					})
				})
			},
		})
		},
		
})
//获取币种信息

//

//获取个人订单信息
myorder()
function myorder(){
	$.ajax({
		url:"https://rest.glenbit.com/order/open/" + wsexchange,
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success:function(data){
			buyhtml = '';
			sellhtml = '';
			if(data.errors != undefined){
				//mui.toast(data.errors[0].msg)
				if(data.errors[0].code == 4000){
					$('.pleaselogin').css('display','block')
				}
				return;
			}else{
				$('.pleaselogin').css('display','none')
			}
			function sortId(a,b){  
			   return b.createdAt-a.createdAt  
			}
			data.sort(sortId);
			
			if(data == ''){
				//$('#refineul').html('<li class="refinelist"><div class="nomore">'+get_lan("Nomore")+'</div></li>')
			}else{
				for(var i = 0 ; i < data.length ; i++){
					if(data[i].type == 'buy'){
						buyhtml +='<li>'+
									'<div class="Entrustulcontent">'+
										'<div>'+data[i].price.toFixed(6)+'</div>'+
										'<div>'+data[i].amount.toFixed(4)+'</div>'+	
									'</div>'+
									'<span class="Entrustid" style="display: none;">'+data[i].orderId+'</span>'+
									'<span class="Entrusttype" style="display: none;">'+data[i].type+'</span>'+
									'<div class="Entrustulcancel">'+get_lan("CANCEL")+'</div>'+
								'</li>'
						}else if(data[i].type == 'sell'){
							sellhtml +='<li>'+
									'<div class="Entrustulcontent">'+
										'<div>'+data[i].price.toFixed(6)+'</div>'+
										'<div>'+data[i].amount.toFixed(4)+'</div>'+	
									'</div>'+
									'<span class="Entrustid" style="display: none;">'+data[i].orderId+'</span>'+
									'<span class="Entrusttype" style="display: none;">'+data[i].type+'</span>'+
									'<div class="Entrustulcancel">'+get_lan("CANCEL")+'</div>'+
								'</li>'
						}
					}
						if(Transactiontype == 'buy' || Transactiontype == undefined){
							$('#Entrustul').html(buyhtml);
						}else if(Transactiontype == 'sell'){
							$('#Entrustul').html(sellhtml);
						}
					}
				
				$("#Entrustul .Entrustulcontent").each(function(){
					$(this).on('tap',function(){
						$(this).siblings('.Entrustulcancel').slideToggle(200);
						$(this).toggleClass('Entrustulcontentbg')
					})
				})
				
			}
	});
}
//撤单
$('body').on('tap','.Entrustulcancel',function(){
	var type = $(this).siblings('.Entrusttype').html();
	var orderId = $(this).siblings('.Entrustid').html();
	console.log(type +','+ orderId)
	$.ajax({
		url: 'https://rest.glenbit.com/order/cancel',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		data: {
			pair: wsexchange,
			orderId: orderId,
			type: type,
		},
		success: (data) =>  {
			if(data.erorrs == undefined){
				myorder();
				$(this).parent().remove();
			}	
		}
	})		
});
 function upcolor(){
	$('.newprice').addClass('is-rising')
	$('.newprice').removeClass('is-dropping')
	$('.LatestCommissionpriceup').css('display','block');
	$('.LatestCommissionpricedown').css('display','none');
	 setTimeout(function(){
		$('.newprice').removeClass('is-rising')
		$('.LatestCommissionpriceup').css('display','none');
	 },1000)
}
function downcolor(){
	$('.newprice').addClass('is-dropping')
	$('.newprice').removeClass('is-rising')
	$('.LatestCommissionpriceup').css('display','none');
	$('.LatestCommissionpricedown').css('display','block');
	 setTimeout(function(){
		$('.newprice').removeClass('is-dropping')
		$('.LatestCommissionpricedown').css('display','none');
	 },1000)
}
//历史订单
$.ajax({
	url: 'https://rest.glenbit.com/order/dealt/' + wsexchange,
	type: 'POST',
	dataType: 'json',
	xhrFields: {
		withCredentials: true
	},
	crossDomain: true,
	data: {
		type: '',
		page: '1',
		limit: 50
	},
	success:(function (data) {
		if(data.errors != undefined){
			//mui.toast(data.errors[0].msg)
			if(data.errors[0].code == 4000){
				$('.pleaselogin').css('display','block')
			}
			return;
		}else{
			$('.pleaselogin').css('display','none')
		}
		if(data.docs == ''){
			
		}else{
			var html = '';
			for(var i = 0 ; i < data.docs.length ; i++){
			html +='<li>'+
						'<div class="Entrustulcontent">'+
							'<div>'+data.docs[i].price.toFixed(6)+'</div>'+
							'<div>'+data.docs[i].amount.toFixed(6)+'</div>'+
						'</div>'+
					'</li>'
			}
			$('#myDealul').html(html);
			}
		
	}),
	error:function(){
		mui.toast(get_lan('FAILURE'))
	}
	});
//socket
	var lockReconnect = false;//避免重复连接
    var wsUrl= "wss://stream-main.glenbit.com/"+ wsexchange;
    var ws;
    var tt;
	var lockReconnect = false;//避免重复连接

    var tt;
    function createWebSocket() {
      try {
        ws = new WebSocket(wsUrl);
        init();
      } catch(e) {
        console.log('catch');
        reconnect(wsUrl);
      }
    }
    function init() {
	var wsnumber = 0;
      ws.onclose = function () {
        console.log('链接关闭');
        reconnect(wsUrl);
      };
      ws.onerror = function() {
        console.log('发生异常了');
        reconnect(wsUrl);
      };
      ws.onopen = function () {
        //心跳检测重置
        heartCheck.start();
      };
      ws.onmessage = function (event) {
        //拿到任何消息都说明当前连接是正常的
		//console.log(event.data)
		if(event.data != 'K' || event.data != 'ping'){
			
		}
		//console.log(event.data)
		
       // console.log(JSON.parse(event.data));
		//wsnumber++;
		 // console.log(wsnumber)
		
		if(event.data == 'ping'){
			  ws.send('pong')
		}else{
			datajsons(event.data);  
		}
		heartCheck.start();
      }
    }
    function reconnect(url) {
      if(lockReconnect) {
        return;
      };
      lockReconnect = true;
      //没连接上会一直重连，设置延迟避免请求过多
      tt && clearTimeout(tt);
      tt = setTimeout(function () {
        createWebSocket(url);
        lockReconnect = false;
      }, 4000);
    }
    //心跳检测
    var heartCheck = {
      timeout: 60000,
      timeoutObj: null,
      serverTimeoutObj: null,
      start: function(){
        //console.log('start');
        var self = this;
        this.timeoutObj && clearTimeout(this.timeoutObj);
        this.serverTimeoutObj && clearTimeout(this.serverTimeoutObj);
        this.timeoutObj = setTimeout(function(){
          //这里发送一个心跳，后端收到后，返回一个心跳消息，
         // console.log('55555');
          ws.send("hb");
          self.serverTimeoutObj = setTimeout(function() {
            //console.log(111);
           // console.log(ws);
            ws.close();
            // createWebSocket();
          }, self.timeout);

        }, this.timeout)
      }
    }
    createWebSocket(wsUrl);
	//
	function datajsons(data){
		  jsonarr = JSON.parse(data);
		  getJsonLength(jsonarr)
	}
	function getJsonLength(jsonData){
		var jsonLength = 0;
		var buyshtml = '';
		var sellshtml = '';
		var history = '';
		socket++;
		if(socket == 1){
			$('.buypriceinput').val(jsonData.sells[0].price); 
			$('.sellpriceinput').val(jsonData.buys[0].price)
		}
		//console.log(newpricehtml);
		
		if(jsonData.buys.length > 5){
			jsonData.buys.length = 5;
		}else{
			jsonData.buys.length = jsonData.buys.length;
		}
		if(jsonData.sells.length > 5){
			jsonData.sells.length = 5;
		}else{
			jsonData.sells.length = jsonData.sells.length;
		}
		//console.log(jsonData.histories)
		var buysprice = [];
		for(var i = 0 ; i < jsonData.buys.length; i++){
			buysprice.push(jsonData.buys[i].price);
			buyshtml += '<li>'+
				'<div class="controlinputprice">'+(jsonData.buys[i].price).toFixed(6)+'</div>'+
				'<div>'+(jsonData.buys[i].amount).toFixed(3)+'</div>'+
			'</li>'
			// console.log(jsonData.buys[])
		}
		//
		
		$("#latestbuyul").html(buyshtml);
		var sellsprice = [];
		 function sortId(a,b){  
		   return b.price-a.price  
		}
		jsonData.sells.sort(sortId);
		for(var i = 0 ; i < jsonData.sells.length; i++){
			//console.log(jsonData.sells[i]);
			if(jsonData.sells[i] != undefined){
				sellsprice.push(jsonData.sells[i].price);
				sellshtml += '<li>'+
					'<div class="controlinputprice">'+(jsonData.sells[i].price).toFixed(6)+'</div>'+
					'<div>'+(jsonData.sells[i].amount).toFixed(3)+'</div>'+
				'</li>'
			}
			// console.log(jsonData.buys[])
		}
		//console.log(depthsellarr)
		/////
		$("#latestsellul").html(sellshtml);
		//console.log(jsonData.histories);
		var historynumber = 0 ;
		var bigorsmall = '';
		$('#Latestdeallistul').html('');
		var Latestdeallistul = document.getElementById('Latestdeallistul').getElementsByTagName("li");
		for(var i = 0 ; i < jsonData.histories.length -1; i ++){
			for(var j = 1; j < jsonData.histories.length; j++){
				if(Number(i+1) == j){
					if(jsonData.histories[i].price >= jsonData.histories[j].price){
						//console.log(111)
						bigorsmall = '<div class="is-rising controlinputprice">'+(jsonData.histories[i].price).toFixed(6)+'</div>';
					}else if(jsonData.histories[i].price < jsonData.histories[j].price){
						bigorsmall = '<div class="is-dropping controlinputprice">'+(jsonData.histories[i].price).toFixed(6)+'</div>';
						//console.log(Latestdeallistul.length)	
					}
				}
				
				
			}
			history += '<li>'+
				bigorsmall+'<div>'+jsonData.histories[i].amount.toFixed(3)+'</div>'+
			'</li>'
			//console.log(Latestdeallistul[i].children[0].innerHTML)
			
			
			historynumber = jsonData.histories[i].price;
// 			history += '<li class="buylist">'+
// 				'<span class="historyamounts">'+ now.toTimeString().substr(0, 8)+'</span>'+
// 				'<span class="tradehistorypricenegative">'+(jsonData.histories[i].price).toFixed(6)+'</span>'+
// 				'<span class="buyvomone">'+jsonData.histories[i].amount.toFixed(3)+'</span>'+
// 			'</li>'
			// console.log(jsonData.buys[])
		}
		$('#Latestdeallistul').html(history);
		
// 		for(var item in jsonData){
// 			console.log(jsonData[item])
// 		}
	}
	
//涨跌幅
var listlockReconnect = false;//避免重复连接
    var listwsUrl = "wss://stream-tickers.glenbit.com/websocket";
    var listws;
    var listtt;
    function createWebSocketone() {
      try {
        listws = new WebSocket(listwsUrl);
        initone();
      } catch(e) {
        console.log('catch');
        reconnectone(listwsUrl);
      }
    }
    function initone() {
	var listwsnumber = 0;
      listws.onclose = function () {
        console.log('链接关闭');
        reconnectone(listwsUrl);
      };
      listws.onerror = function() {
        console.log('发生异常了');
        reconnectone(listwsUrl);
      };
      listws.onopen = function () {
        //心跳检测重置
        heartCheck.start();
      };
      listws.onmessage = function (event) {
        //拿到任何消息都说明当前连接是正常的
        //console.log(event.data);
		listwsnumber++;
		 // console.log(wsnumber)
		  if(listwsnumber == 1){
			 datajsonstwo(event.data);
		  }else{
			  jsonLengthtwo(event.data);
		  }
		if(event.data == 'ping'){
			  listws.send('pong')
		}
		heartCheck.start();
      }
    }
    function reconnectone(url) {
      if(listlockReconnect) {
        return;
      };
      listlockReconnect = true;
      //没连接上会一直重连，设置延迟避免请求过多
      listtt && clearTimeoutone(tt);
      listtt = setTimeout(function () {
        createWebSocketone(url);
        listlockReconnect = false;
      }, 4000);
    }
    //心跳检测
    var heartCheck = {
      timeout: 30000,
      timeoutObj: null,
      serverTimeoutObj: null,
      start: function(){
        //console.log('start');
        var self = this;
        this.timeoutObj && clearTimeout(this.timeoutObj);
        this.serverTimeoutObj && clearTimeout(this.serverTimeoutObj);
        this.timeoutObj = setTimeout(function(){
          //这里发送一个心跳，后端收到后，返回一个心跳消息，
         // console.log('55555');
          listws.send("hb");
          self.serverTimeoutObj = setTimeout(function() {
            //console.log(111);
           // console.log(ws);
            listws.close();
            // createWebSocket();
          }, self.timeout);

        }, this.timeout)
      }
    }
    createWebSocketone(listwsUrl);
	
	function datajsonstwo(data){
			  var jsonarr = JSON.parse(data);
			  getJsonLengthtwo(jsonarr)
	}
	function jsonLengthtwo(datatwo){
			  if(datatwo != 'ping'){
				 jsonarrindex = JSON.parse(datatwo); 
				// console.log(jsonarrindex);
				if(jsonarrindex.pair == wsexchange){
					$('.Upsanddowns').html((jsonarrindex.change * 100).toFixed(2) + '%')
					if(jsonarrindex.change < 0){
						$('.Upsanddowns').css('color','#F04A5D')
					}else if(jsonarrindex.change >= 0){
						$('.Upsanddowns').css('color','#00ACDC')
					}
					var newpricehtml = $('.newprice').html();
					if(jsonarrindex.close > newpricehtml){
						upcolor();
						$('.newprice').html(jsonarrindex.close);
					}else if(jsonarrindex.close < newpricehtml){
						downcolor();
						$('.newprice').html(jsonarrindex.close);
					}
				}
				}
	}
	function getJsonLengthtwo(jsonData){	
			for(var item in jsonData){
				if(jsonData[item].pair == wsexchange){
					$('.Upsanddowns').html((jsonData[item].change * 100).toFixed(2) + '%')
					if(jsonData[item].change < 0){
						$('.Upsanddowns').css('color','#F04A5D');
					}else if(jsonData[item].change > 0){
					$('.Upsanddowns').css('color','#00ACDC');
					}
					var newpricehtml = $('.newprice').html();
					if(jsonData[item].close > newpricehtml){
						upcolor();
						$('.newprice').html(jsonData[item].close);
					}else if(jsonData[item].close < newpricehtml){
						downcolor();
						$('.newprice').html(jsonData[item].close);
					}
				}
			}
		}

