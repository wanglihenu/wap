# GlenBit API ドキュメント

## REST API (Base Url: `https://rest.glenbit.com`)

### ERROR_CODE
```
enum ErrorType {
  UNKNOWN = 9999,
  REACH_API_LIMIT = 9998,
  VERIFY_CODE_UNMATCHED = 9997,
  INVALID_PARAMETERS = 9996,

  // master data
  INVALID_COIN_PAIR = 3000,
  UNSUPPORT_COIN = 3001,

  // account related
  UNAUTHENTICATED = 4000,
  DUPLICATE_REGISTRATION = 4001,
  INVALID_USER_INFO = 4002,
  INACTIVATED_USER = 4003,
  GEETEST_FAILURE = 4004,
  ACTIVATION_FAILURE = 4005,
  TWO_STEP_FAILURE = 4006,

  // Verification related
  SEND_SMS_VERIFY_CODE_FAILURE = 4101,
  DUPLICATE_VERIFICATION = 4102,

  // order related
  MAKE_ORDER_FAILURE = 5000,
  INSUFFICIENT_BALANCE_FOR_ORDER = 5001,
  ILEGAL_ORDER_OPERATION = 5002,

  // wallet related
  UNFOUND_COIN = 6000,
  INVALID_ADDRESS = 6001,
  WITHDRAW_FALIURE = 6002,
  INVALID_WITHDRAW_AMOUNT = 6003,
}
```

### Open

#### Coins
- Endpoint: `/coins`-----------------------------币种，平台有哪些币（附带基本信息）提币充值要用
- Method: `GET`
- Response: `
    [
        {
            _id: 'BTC',
            name: 'Bitcoin',
            minWithdraw: 0.00,
            withdrawPrecision: 8,
            fee: 0.00,
            sortOrder: 2,
            canDeposit: true,
            canWithdraw: true
        },
        {
            ...
        }
    ]
`

#### Markets
- Endpoint: `/markets`-----------------------------------交易对基本信息，首页要用
- Method: `GET`
- Response: `
    [
        {
            marketCoin: 'BTC',
            targetCoin: 'ETH',
            fee: 0.002,
            precision: 6,
            minVolume: 0.0001,
            isAvailable: true
        },
        {
            ...
        }
    ]
`

#### Market Price
- Endpoint: `/market_prices`---------------------------------------------市值（from=多少个to）个人资产总估值的
- Method: `GET`
- Response: `
    [
        {
            from: 'BTC',
            to: 'USDT',
            price: ???.??,
            isLocal: false,
            updatedAt: ???????????
        },
        {
            ...
        }
    ]
`

### Account

#### Log out
- Endpint: `/account/logout`---------------------------退出
- Method: `POST`
- Parameters: NONE
- Response: `"Successfully logged out."`


#### Sign up with Email
- Endpint: `/account/signup`---------------------------邮箱注册
- Method: `POST`
- Parameters:
    - email: string
    - password: string
    - lang: 'zh_CN' | 'en_US'
    - geetest_challenge: 详见Geetest官方文档
    - geetest_validate: 详见Geetest官方文档
    - geetest_seccode: 详见Geetest官方文档
    - referralCode: string, `optional`
- Responses:
    - success: `"Successfully signed up, please check your mailbox"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Sign up with Phone
- Endpint: `/account/signup_phone`---------------------------手机号码注册
- Method: `POST`
- Parameters:
    - countryCode: string
    - phone: string
    - password: string
    - code: string (短信中的验证码)
    - geetest_challenge: 详见Geetest官方文档
    - geetest_validate: 详见Geetest官方文档
    - geetest_seccode: 详见Geetest官方文档
    - referralCode: string, `optional`
- Responses:
    - success: `"Successfully signed up, please login with your Phone number"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Login
- Endpint: `/account/login`---------------------------邮箱/手机登录
- Method: `POST`
- Parameters:
    - email: string (用户的邮箱地址或完整手机号)
    - password: string
    - geetest_challenge: 详见Geetest官方文档
    - geetest_validate: 详见Geetest官方文档
    - geetest_seccode: 详见Geetest官方文档
- Responses:
    - success: `
    {
        email: 用户的邮箱地址或完整手机号,
        isTwoStepEnabled: true | false,-----谷歌二次认证显示是否打开（短信认证是否打开另外接口提供）
        recentLogins: [ { ip: xx.xx.xx.xx, createdAt: ????? }, {...} ]
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Login if Google Two-Step Authentication is turned ON-----------------------谷歌二次认证打开后的登录（geetest认证后输入google认证）
- Endpoint: `/account/login_ga`
- Method: `POST`
- Parameters:
    - email: string (用户的邮箱地址或完整手机号)
    - password: string
    - token: string (Google Two-Step Authentication code)
- Responses:
    - success: `
    {
        email: user's email,
        isTwoStepEnabled: true | false,
        recentLogins: [ { ip: xx.xx.xx.xx, createdAt: ????? }, {...} ]
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Send SMS verify code-------------------------------------------发送验证码
- Endpoint: `/account/sms_verify_code`
- Method: `POST`
- Parameters:
    - countryCode: string,
    - phone: string
- Responses:
    - success: `"SMS verify code was sent successfully"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `


#### Verify phone number-------------------------------------------验证码校验
- `NEED LOGIN`
- Endpoint: `/account/verify_phone`
- Method: `POST`
- Parameters:
    - countryCode: string
    - phone: string
    - code: string (短信中的验证码)
- Responses:
    - success: `"Phone successfully verified!"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Try to reset password by Email-------------------------------------------修改密码邮箱验证码发送
- Endpoint: `/account/try_reset_password`
- Method: `POST`
- Parameters:
    - email: string, `optional if already logged in`
- Responses:
    - success: `"reset code sent!"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Actually reset password by Email-------------------------------------------通过邮箱修改密码
- Endpoint: `/account/reset_password`
- Method: `POST`
- Parameters:
    - email: string
    - password: string
    - code: string (短信中的验证码)
    - geetest_challenge: 详见Geetest官方文档
    - geetest_validate: 详见Geetest官方文档
    - geetest_seccode: 详见Geetest官方文档
- Responses:
    - success: `"Password reset successfully, please login again"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Try to reset password by Phone------------------------------------------修改密码手机验证码发送
- Endpoint: `/account/try_reset_password_phone`
- Method: `POST`
- Parameters:
    - countryCode: string, `optional if already logged in`
    - phone: string, `optional if already logged in`
- Responses:
    - success: `"reset code sent!"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Actually reset password by Phone------------------------------------------通过手机修改密码
- Endpoint: `/account/reset_password_phone`
- Method: `POST`
- Parameters:
    - countryCode: string, `optional`
    - phone: string, `optional`
    - password: string
    - code: string (短信中的验证码)
    - geetest_challenge: 详见Geetest官方文档
    - geetest_validate: 详见Geetest官方文档
    - geetest_seccode: 详见Geetest官方文档
- Responses:
    - success: `"Password reset successfully, please login again"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Modify password by Email (for logged in users)------------------------------------------通过手机修改密码
- `NEED LOGIN`
- Endpoint: `/account/modify_password`
- Method: `POST`
- Parameters:
    - password: string
    - code: string (短信中的验证码)
- Responses:
    - success: `"Password reset successfully, please login again"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Modify password by Email (for logged in users)
- `NEED LOGIN`
- Endpoint: `/account/modify_password_phone`-------------------------------------修改登录密码，到底是google中的验证码还是短信中的验证码，不是谷歌的
- Method: `POST`
- Parameters:
    - password: string
    - code: string (短信中的验证码)
- Responses:
    - success: `"Password reset successfully, please login again"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `


#### Reset Google Two-step Authentication
- `NEED LOGIN`
- Endpoint: `/account/reset_two_step`------------------------------------------谷歌二次认证发送验证码
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `"reset code sent!"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Verify SMS code for Google Two-step Authentication------------------------------------------谷歌二次认证验证码校验
- `NEED LOGIN`
- Endpoint: `/account/twp_step_verify_code`
- Method: `POST`
- Parameters:
    - code: string (短信中的验证码)
- Responses:
    - success: `
    {
        key: GOOGLE_TWO_STEP_SECRET,
        url: OTP_AUTH_URL_FOR_GOOGLE_TWO_STEP_SECRET
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Enable Google Two-step Authentication--------------------------打开谷歌二次认证，验证码验证
- `NEED LOGIN`
- Endpoint: `/account/enable_twp_step`
- Method: `POST`
- Parameters:
    - token: string (Google Two-Step Authentication code)
- Responses:
    - success: `"Two-step authentication enabled!"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Disable Google Two-step Authentication--------------------------关掉（打开了走关闭，关闭了的话点击走打开的流程）
- `NEED LOGIN`
- Endpoint: `/account/disable_twp_step`
- Method: `POST`
- Parameters:
    - token: string (Google Two-Step Authentication code)
- Responses:
    - success: `"Two-step authentication disabled!"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

### Trade (NEED LOGIN)

#### Make order
- Endpoint: `/order/make`----------------------下单
- Method: `POST`
- Parameters:
    - pair: 'ETH-BTC' | 'XRP-USDT'| ...
    - type: 'buy' | 'sell'
    - price: number
    - amount: number
- Responses:
    - success: `
    {
        type: 'buy' | 'sell',
        price:
        amount:
        createdAt:
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Cancel order--------------------------------------------------取消订单
- Endpoint: `/order/cancel`
- Method: `POST`
- Parameters:
    - pair: 'ETH-BTC' | 'XRP-USDT'| ...
    - orderId: string
    - type: 'buy' | 'sell'
- Responses:
    - success: `"orderId"`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### All open orders
- Endpoint: `/order/open`-------------------------当前的所有委托（挂单）
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `
    [
        {
            orderId:
            pair:
            type:
            price:
            amount:-------待成交
            filled:-------已成交
            createdAt:
        },
        { ... }
    ]
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Open orders for a pair
- Endpoint: `/order/open/:pair`------------------某个交易对的当前委托
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `
    [
        {
            orderId:
            pair:
            type:
            price:
            amount:------待成交
            filled:--------已成交
            createdAt:
        },
        { ... }
    ]
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Dealt orders for a pair-------------------------已成交的记录
- Endpoint: `/order/dealt/:pair`---------'ETH-BTC' (Orders 中的trade history里面一点开是没有记录的，用户需要输入一个交易对才能查看记录)
- Method: `POST`
- Parameters:---少一个参数date
    - type: 'buy' | 'sell' | ''
    - page: ページ数=====pageindex
    - limit: ページサイズ=====pagesize
- Responses:
    - success: `
    {
        docs: [
            {
                pair:
                type: 'buy' | 'sell',
                price:
                amount:
                createdAt:
            },
            { . . . }
        ],
        total:
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `


### Wallet (NEED LOGIN)

#### Balances-----------------------------------------钱包余额,通过market_prices进行换算其他币的等值信息
- Endpoint: `/wallet/balance`-------我的资产
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `
    [
        {
            coin:
            free:
            locked:
        },
        { . . . }
    ]
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Deposit history
- Endpoint: `/wallet/deposit_history`---交易记录
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `
    [
        {
            txid:
            coin:
            amount:
            createdAt:
            udpatedAt:null话为待确认，否则显示在充值记录中
        },
        { . . . }
    ]
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Withdraw history
- Endpoint: `/wallet/withdraw_history`-----提现
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `
    [
        {
            address:
            txid:
            coin:----txid与updateAt为空审核中，只有txid为空的时候为处理中，txid不为空的时候为已汇出。
            amount:
            createdAt:
            updatedAt:
        },
        { . . . }
    ]
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### Get Deposit address for a coin-------------------获取币的地址
- Endpoint: `/wallet/address`
- Method: `POST`
- Parameters:
    - coin: string
- Responses:
    - success: `DEPOSIT_ADDRESS_FOR_THIS_COIN`
    - failure: ``

#### Try to withdraw---------------------------提现当前币的信息
- Endpoint: `/wallet/try_withdraw`
- Method: `POST`
- Parameters:
    - coin: string
- Responses:
    - success: `
    {
        fee:
        minWithdraw:
        dailyAvailable:
        available:
        balance:
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `


#### Actually withdraw-----------------------提现接口
- Endpoint: `/wallet/withdraw`
- Method: `POST`
- Parameters:
    - address: string
    - coin: string
    - amount: number
    - token: string (Google Two-Step Authentication code)
- Responses:
    - success: `WITHDRAW_ID`
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

### Referral Commissions (NEED LOGIN)

#### My referral info------------------------邀请返利
- Endpoint: `/referral/info`
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `
    {
        referralCode:864724196
        commissionRate:返佣比例
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `
#### My Invitees
- Endpoint: `/referral/invitations`---我的邀请用户列表
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `
    {
        name:
        createdAt:
        updatedAt:
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

#### My Commissions-----------------------佣金记录
- Endpoint: `/referral/commissions`
- Method: `POST`
- Parameters: NONE
- Responses:
    - success: `
    {
        type: 'AsInviter' | 'AsInvitee'
        coin:
        fromUser:
        amount:
        updatedAt:
    }
    `
    - failure: `
        { errors: [ code: ERROR_CODE, msg: 'error message' ] }
    `

### Other

### K-line
- url: `https://tv-feeder.glenbit.com/history`

更新待ち・・

## WebSocket API

### Tickers
- url: `wss://stream-tickers.glenbit.com/websocket`


### TradingInfo
- url: `wss://stream-main.glenbit.com/`+交易对

更新待ち・・
