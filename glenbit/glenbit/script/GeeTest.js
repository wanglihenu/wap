//在页面上需要引入
//<script src="http://libs.baidu.com/jquery/1.9.0/jquery.js"></script>
//<script src="http://static.geetest.com/static/tools/gt.js"></script>
//举例
//<div style="margin:300px;" >
//  <div id="yzm"></div>
//  <button type="submit" id="login">Login</button>
//</div>
//js部分
var handler = function(captchaObj) {
	captchaObj.appendTo("#yzm");
};
$.ajax({
	url: "GeetestCaptcha.aspx?action=Create"  + (new Date()).getTime(), // 加随机数防止缓存
	type: "get",
	dataType: "json",
	success: function(data) {
		//使用initGeetest接口
		// 参数1：配置参数
        // 参数2：回调，回调的第一个参数验证码对象，之后可以使用它做appendTo之类的事件
		initGeetest({
			gt: data.gt,
			challenge: data.challenge,
			product: "bind",//设置下一步验证的展现形式 ，float、popup、custom、bind
			offline: !data.success// 表示用户后台检测极验服务器是否宕机，与SDK配合，用户一般不需要关注
			},
			handler
		);
	}
});
$("#login").click(function() {
	$.ajax({
		url: "GeetestCaptcha.aspx?action=Check",
		type: "post",
		data: {
			geetest_challenge: $(".geetest_challenge").val(),
			geetest_validate: $(".geetest_validate").val(),
			geetest_seccode: $(".geetest_seccode").val()
		},
		success: function(data) {
			if(data == "ok") {
				alert("ok");
			}
		}
	})
})