'use strict';
/*
	This class implements interaction with UDF-compatible datafeed.

	See UDF protocol reference at
	https://github.com/tradingview/charting_library/wiki/UDF
*/
var Datafeeds = {};
Datafeeds.UDFCompatibleDatafeed = function(locale, updateFrequency) {
	console.log(1);
	this._locale = locale;
	this._configuration = undefined;
	this._symbolSearch = null;
	this._symbolsStorage = null;
	this._barsPulseUpdater = new Datafeeds.DataPulseUpdater(this, updateFrequency || 10 * 1000);
	//this._quotesPulseUpdater = new Datafeeds.QuotesPulseUpdater(this);

	this._enableLogging = true;
	this._initializationFinished = false;
	this._callbacks = {};
	this._currentSubscriptionRecord = null;
	this._socket = null;

	this._initialize();
};

Datafeeds.UDFCompatibleDatafeed.prototype.defaultConfiguration = function() {
	console.log(2);
	return {
		supports_search: false,
		supports_group_request: true,
		supported_resolutions: ['1','5','15',"30","60","180","360","720",'1440'],
		supports_marks: false,
		supports_timescale_marks: false
	};
};

Datafeeds.UDFCompatibleDatafeed.prototype.getServerTime = function(callback) {
	console.log(3);
	if(this._configuration.supports_time) {

		this._send({
			method: "server.time",
			params: []
		}, function(res) {
			if(!res.error) {
				callback(res.result);
			}
		})
	}
};

Datafeeds.UDFCompatibleDatafeed.prototype.on = function(event, callback) {
	console.log(4);
	if(!this._callbacks.hasOwnProperty(event)) {
		this._callbacks[event] = [];
	}

	this._callbacks[event].push(callback);
	return this;
};

Datafeeds.UDFCompatibleDatafeed.prototype._fireEvent = function(event, argument) {
	console.log(5);
	if(this._callbacks.hasOwnProperty(event)) {
		var callbacksChain = this._callbacks[event];
		for(var i = 0; i < callbacksChain.length; ++i) {
			callbacksChain[i](argument);
		}

		this._callbacks[event] = [];
	}
};
Datafeeds.UDFCompatibleDatafeed.prototype.onInitialized = function() {
	this._initializationFinished = true;
	this._fireEvent('initialized');
};

Datafeeds.UDFCompatibleDatafeed.prototype._logMessage = function(message) {
	if(this._enableLogging) {
		var now = new Date();
		console.log(now.toLocaleTimeString() + '.' + now.getMilliseconds() + '> ' + message);
	}
};
//发送请求
Datafeeds.UDFCompatibleDatafeed.prototype._send = function(url, params, callback) {
	// console.log(params)
	var request = url;
	// 传递的参数
	if (params) {
		for (var i = 0; i < Object.keys(params).length; ++i) {
			var key = Object.keys(params)[i];
			var value = encodeURIComponent(params[key]);
			request += (i === 0 ? '?' : '&') + key + '=' + value;
		}
	}
	//提示语
	this._logMessage('New request: ' + request);
	//发送请求
	return $.ajax({
		type: 'GET',
		url: request,
		contentType: 'text/plain'
	});
};
Datafeeds.UDFCompatibleDatafeed.prototype._initialize = function() {
	var that = this;
	this._setupWithConfiguration({
		"supports_search": false,
		"supports_group_request": false,
		"supports_marks": false,
		"supports_timescale_marks": false,
		"supports_time": false,
		"exchanges": [{
			"value": "weex",
			"name": "weex",
			"desc": "weex exchange"
		}],
		"symbolsTypes": [{
			"name": "bitcoin",
			"value": "bitcoin"
		}],
		"supportedResolutions": ['1','5','15',"30","60","180","360","720",'1D']
	})
};
Datafeeds.UDFCompatibleDatafeed.prototype.onReady = function(callback) {
	console.log(9);
	var that = this;
	if(this._configuration) {
		setTimeout(function() {
			callback(that._configuration);
		}, 0);
	} else {
		this.on('configuration_ready', function() {
			callback(that._configuration);
		});
	}
};

Datafeeds.UDFCompatibleDatafeed.prototype._setupWithConfiguration = function(configurationData) {
	console.log(10);
	this._configuration = configurationData;

	if(!configurationData.exchanges) {
		configurationData.exchanges = [];
	}

	//	@obsolete; remove in 1.5
	var supportedResolutions = configurationData.supported_resolutions || configurationData.supportedResolutions;
	configurationData.supported_resolutions = supportedResolutions;

	//	@obsolete; remove in 1.5
	var symbolsTypes = configurationData.symbols_types || configurationData.symbolsTypes;
	configurationData.symbols_types = symbolsTypes;

//	if(!configurationData.supports_search && !configurationData.supports_group_request) {
//		throw new Error('Unsupported datafeed configuration. Must either support search, or support group request');
//	}

//	if(!configurationData.supports_search) {
////		this._symbolSearch = new Datafeeds.SymbolSearchComponent(this);
//	}

	if(configurationData.supports_group_request) {
		//	this component will call onInitialized() by itself
		this._symbolsStorage = new Datafeeds.SymbolsStorage(this);
	} else {
		this.onInitialized();
	}

	this._fireEvent('configuration_ready');
	this._logMessage('Initialized with ' + JSON.stringify(configurationData));
};

//	===============================================================================================================================
//	The functions set below is the implementation of JavaScript API.

Datafeeds.UDFCompatibleDatafeed.prototype.getMarks = function(symbolInfo, rangeStart, rangeEnd, onDataCallback, resolution) {
	//console.log(resolution)
	 if (this._configuration.supports_marks) {
	 	this._send(this._datafeedURL + '/marks', {
	 		symbol: symbolInfo.ticker.toUpperCase(),
	 		from: rangeStart,
	 		to: rangeEnd,
	 		resolution: resolution
	 	})
	 		.done(function(response) {
	 			onDataCallback(JSON.parse(response));
	 		})
	 		.fail(function() {
	 			onDataCallback([]);
	 		});
	 }
};

Datafeeds.UDFCompatibleDatafeed.prototype.getTimescaleMarks = function(symbolInfo, rangeStart, rangeEnd, onDataCallback, resolution) {
	 if (this._configuration.supports_timescale_marks) {
	 	this._send(this._datafeedURL + '/timescale_marks', {
	 		symbol: symbolInfo.ticker.toUpperCase(),
	 		from: rangeStart,
	 		to: rangeEnd,
	 		resolution: resolution
	 	})
	 		.done(function(response) {
	 			onDataCallback(JSON.parse(response));
	 		})
	 		.fail(function() {
	 			onDataCallback([]);
	 		});
	 }
};

Datafeeds.UDFCompatibleDatafeed.prototype._symbolResolveURL = '/symbols';

//	BEWARE: this function does not consider symbol's exchange

Datafeeds.UDFCompatibleDatafeed.prototype.resolveSymbol = function(symbolName, onSymbolResolvedCallback, onResolveErrorCallback) {
	var that = this;
	if(!this._initializationFinished) {
		this.on('initialized', function() {
			that.resolveSymbol(symbolName, onSymbolResolvedCallback, onResolveErrorCallback);
		});
		return;
	}
	var resolveRequestStartTime = Date.now();
	that._logMessage('Resolve requested');
	function onResultReady(data) {
		var postProcessedData = data;
		if(that.postProcessSymbolInfo) {
			postProcessedData = that.postProcessSymbolInfo(postProcessedData);
		}

		that._logMessage('Symbol resolved: ' + (Date.now() - resolveRequestStartTime));

		onSymbolResolvedCallback(postProcessedData);
	}
	//从其他地方传递过来的值
	if(localStorage.getItem("Name")){
		var syswed = localStorage.getItem("Name")
	}else{
		var syswed = 'BTC-USDT'
	}
	if(!this._configuration.supports_group_request) {
		setTimeout(function() {
			var dest = /dest=(.*?)($|&)/.exec(location.search)
			if(dest && dest.length) {
				dest = dest[1]
			}
			var currency = /currency=(.*?)($|&)/.exec(location.search)
			if(currency && currency.length) {
				currency = currency[1]
			}
			onResultReady({
				name: syswed,//商品名称
				market: syswed,	//市场
				"exchange-traded": "",
				"exchange-listed": "",
				timezone: "Asia/Shanghai",
				minmov: 1,
				minmov2: 0,
				pricescale: 1000,
				volume_precision:6,
				pointvalue: 1,
				fractional: false,
				session: "24x7",
				has_intraday: true,
				has_no_volume: false,
				ticker: syswed,//股票代号
				description: syswed,// apple inc显示的地方
				type: "bitcoin",
				supported_resolutions: [
					"1",
					"5",
					"15",
					"30",
					"60",
					"180",
					"360",
					"720",
					'1D'
				]
			})
		}, 1)
	} else {
		if(this._initializationFinished) {
			this._symbolsStorage.resolveSymbol(symbolName, onResultReady, onResolveErrorCallback);
		} else {
			this.on('initialized', function() {
				that._symbolsStorage.resolveSymbol(symbolName, onResultReady, onResolveErrorCallback);
			});
		}
	}
};
Datafeeds.UDFCompatibleDatafeed.prototype._historyURL = '/history';
Datafeeds.UDFCompatibleDatafeed.prototype.getBars = function(symbolInfo, resolution, rangeStartDate, rangeEndDate, onDataCallback, onErrorCallback) {
	console.log( rangeStartDate, rangeEndDate)
	if(rangeStartDate > 0 && (rangeStartDate + '').length > 10) {
		throw new Error(['Got a JS time instead of Unix one.', rangeStartDate, rangeEndDate]);
	}
	console.log(resolution)
	if (!resolution.endsWith('D') ) {
      let frequency = parseInt(resolution);
    }
	let arr = symbolInfo.name.split('/');
	let endpoint = resolution.endsWith('D') ? '/day' : parseInt(resolution, 10) >= 60 ? '/hour' : '/minute';
	var param = {"pair":  symbolInfo.name,"start": rangeStartDate,"end": rangeEndDate};
	//console.log(param)
	//console.log('https://tv-feeder.glenbit.com/history'+ endpoint)
	$.ajax({
		type:'POST',
		data:param,
		url:'https://tv-feeder.glenbit.com/history'+ endpoint,
		success:function(data){
			//console.log(data)
			var res =  data ;
			if (res.length === 0) return [];
			let bars = [];
			var nodata = res.length === 0;
			for(var i = 0 ;i<res.length;i++){
				var Bar = {
					time: res[i]._id,
			        open: res[i].open,
			        high: res[i].high,
			        low: res[i].low,
			        close: res[i].close,
			        volume: res[i].volume
				};
				bars.push(Bar)
			}
			onDataCallback(bars, {
				noData: nodata
			});
		}
	})
};
Datafeeds.UDFCompatibleDatafeed.prototype.subscribeBars = function(symbolInfo, resolution, onRealtimeCallback, listenerGUID, onResetCacheNeededCallback) { 
	this._barsPulseUpdater.subscribeDataListener(symbolInfo, resolution, onRealtimeCallback, listenerGUID, onResetCacheNeededCallback);
};

Datafeeds.UDFCompatibleDatafeed.prototype.unsubscribeBars = function(listenerGUID) {
	// alert('unsubscribeBars:  '+listenerGUID);
	console.log('333')
	this._barsPulseUpdater.unsubscribeDataListener(listenerGUID);
};

Datafeeds.UDFCompatibleDatafeed.prototype.calculateHistoryDepth = function(period, resolutionBack, intervalBack) {};
 
Datafeeds.SymbolsStorage = function (datafeed) {
    this._datafeed = datafeed;
    this._exchangesList = ['NYSE', 'FOREX', 'AMEX'];
    this._exchangesWaitingForData = {};
    this._exchangesDataCache = {};
    this._symbolsInfo = {};
    this._symbolsList = [];
    this._requestFullSymbolsList();
};
Datafeeds.SymbolsStorage.prototype._requestFullSymbolsList = function () {
    var that = this;
    for (var i = 0; i < this._exchangesList.length; ++i) {
    var exchange = this._exchangesList[i];
        if (this._exchangesDataCache.hasOwnProperty(exchange)) {
           continue;
        }
           this._exchangesDataCache[exchange] = true;

           this._exchangesWaitingForData[exchange] = 'waiting_for_data';

           this._datafeed._send(this._datafeed._datafeedURL + '/symbol_info', {
               group: exchange
           })
               .done((function (exchange) {
                   return function (response) {
                       that._onExchangeDataReceived(exchange, JSON.parse(response));
                       that._onAnyExchangeResponseReceived(exchange);
                   };
               })(exchange))
               .fail((function (exchange) {
                   return function (reason) {
                       that._onAnyExchangeResponseReceived(exchange);
                   };
               })(exchange));
       }
   };
Datafeeds.SymbolsStorage.prototype._onExchangeDataReceived = function (exchangeName, data) {
	
       function tableField(data, name, index) {
           return data[name] instanceof Array ?
               data[name][index] :
               data[name];
       }

       try {
           for (var symbolIndex = 0; symbolIndex < data.symbol.length; ++symbolIndex) {
               var symbolName = data.symbol[symbolIndex];
               var listedExchange = tableField(data, 'exchange-listed', symbolIndex);
               var tradedExchange = tableField(data, 'exchange-traded', symbolIndex);
               var fullName = tradedExchange + ':' + symbolName;
               var hasIntraday = tableField(data, 'has-intraday', symbolIndex);

               var tickerPresent = typeof data.ticker != 'undefined';

               var symbolInfo = {
                   name: symbolName,
                   base_name: [listedExchange + ':' + symbolName],
                   description: tableField(data, 'description', symbolIndex),
                   full_name: fullName,
                   legs: [fullName],
                   has_intraday: hasIntraday,
                   has_no_volume: tableField(data, 'has-no-volume', symbolIndex),
                   listed_exchange: listedExchange,
                   exchange: tradedExchange,
                   minmov: tableField(data, 'minmovement', symbolIndex) || tableField(data, 'minmov', symbolIndex),
                   minmove2: tableField(data, 'minmove2', symbolIndex) || tableField(data, 'minmov2', symbolIndex),
                   fractional: tableField(data, 'fractional', symbolIndex),
                   pointvalue: tableField(data, 'pointvalue', symbolIndex),
                   pricescale: tableField(data, 'pricescale', symbolIndex),
                   type: tableField(data, 'type', symbolIndex),
                   session: tableField(data, 'session-regular', symbolIndex),
                   ticker: tickerPresent ? tableField(data, 'ticker', symbolIndex) : symbolName,
                   timezone: tableField(data, 'timezone', symbolIndex),
                   supported_resolutions: tableField(data, 'supported-resolutions', symbolIndex) || this._datafeed.defaultConfiguration().supported_resolutions,
                   force_session_rebuild: tableField(data, 'force-session-rebuild', symbolIndex) || false,
                   has_daily: tableField(data, 'has-daily', symbolIndex) || true,
                   intraday_multipliers: tableField(data, 'intraday-multipliers', symbolIndex) || ['1', '5', '15', '30', '60',"720","1D"],
                   has_fractional_volume: tableField(data, 'has-fractional-volume', symbolIndex) || false,
                   has_weekly_and_monthly: tableField(data, 'has-weekly-and-monthly', symbolIndex) || false,
                   has_empty_bars: tableField(data, 'has-empty-bars', symbolIndex) || false,
                   volume_precision: tableField(data, 'volume-precision', symbolIndex) || 0
               };

               this._symbolsInfo[symbolInfo.ticker] = this._symbolsInfo[symbolName] = this._symbolsInfo[fullName] = symbolInfo;
               this._symbolsList.push(symbolName);
           }
       } catch (error) {
           throw new Error('API error when processing exchange `' + exchangeName + '` symbol #' + symbolIndex + ': ' + error);
       }
   };
// 更新数据最新一条的数据
Datafeeds.DataPulseUpdater = function(datafeed, updateFrequency) {
	this._datafeed = datafeed;
	this._subscribers = {};
	this._requestsPending = 0;
	var that = this;
	var update = function() {
		if (that._requestsPending > 0) {
			return;
		}
		for (var listenerGUID in that._subscribers) {
			var subscriptionRecord = that._subscribers[listenerGUID];
			var resolution = subscriptionRecord.resolution;
			//console.log(subscriptionRecord.resolution)
			var datesRangeRight = parseInt((new Date().valueOf()) / 1000);

			//	BEWARE: please note we really need 2 bars, not the only last one
			//	see the explanation below. `10` is the `large enough` value to work around holidays
			var datesRangeLeft = datesRangeRight - that.periodLengthSeconds(resolution, 10);

			that._requestsPending++;
			(function(_subscriptionRecord) { // eslint-disable-line
				that._datafeed.getBars(_subscriptionRecord.symbolInfo, resolution, datesRangeLeft, datesRangeRight, function(bars) {
					that._requestsPending--;
					//	means the subscription was cancelled while waiting for data
					if (!that._subscribers.hasOwnProperty(listenerGUID)) {
						return;
					}
					if (bars.length === 0) {
						return;
					}
					//console.log(bars[bars.length-1])
					var lastBar = bars[bars.length-1];
					if (!isNaN(_subscriptionRecord.lastBarTime) && lastBar.time < _subscriptionRecord.lastBarTime) {
						return;
					}
					var subscribers = _subscriptionRecord.listeners;
					//	BEWARE: this one isn't working when first update comes and this update makes a new bar. In this case
					//	_subscriptionRecord.lastBarTime = NaN
					var isNewBar = !isNaN(_subscriptionRecord.lastBarTime) && lastBar.time > _subscriptionRecord.lastBarTime;
					//	Pulse updating may miss some trades data (ie, if pulse period = 10 secods and new bar is started 5 seconds later after the last update, the
					//	old bar's last 5 seconds trades will be lost). Thus, at fist we should broadcast old bar updates when it's ready.
					if (isNewBar) {
						if (bars.length < 2) {
							throw new Error('Not enough bars in history for proper pulse update. Need at least 2.');
						}
						var previousBar = bars[bars.length - 2];
						for (var i = 0; i < subscribers.length; ++i) {
							subscribers[i](previousBar);
						}
					}
					_subscriptionRecord.lastBarTime = lastBar.time;
					for (var i = 0; i < subscribers.length; ++i) {
						subscribers[i](lastBar);
					}
				},
				function() {
					that._requestsPending--;
				});
			})(subscriptionRecord);
		}
	};

	if (typeof updateFrequency != 'undefined' && updateFrequency > 0) {
		setInterval(update, updateFrequency);
	}
};

Datafeeds.DataPulseUpdater.prototype.unsubscribeDataListener = function(listenerGUID) {
	//alert('unsubscribeDataListener');
	this._datafeed._logMessage('Unsubscribing ' + listenerGUID);
	delete this._subscribers[listenerGUID];
};

Datafeeds.DataPulseUpdater.prototype.subscribeDataListener = function(symbolInfo, resolution, newDataCallback, listenerGUID) {
	// alert('subscribeDataListener');
	this._datafeed._logMessage('Subscribing ' + listenerGUID);

	if(!this._subscribers.hasOwnProperty(listenerGUID)) {
		this._subscribers[listenerGUID] = {
			symbolInfo: symbolInfo,
			resolution: resolution,
			lastBarTime: NaN,
			listeners: []
		};
	}
	this._subscribers[listenerGUID].listeners.push(newDataCallback);
};
Datafeeds.DataPulseUpdater.prototype.periodLengthSeconds = function(resolution, requiredPeriodsCount) {
	var daysCount = 0;
	if(resolution === 'D') {
		daysCount = requiredPeriodsCount;
	} else if(resolution === 'M') {
		daysCount = 31 * requiredPeriodsCount;
	} else if(resolution === 'W') {
		daysCount = 7 * requiredPeriodsCount;
	} else {
		daysCount = requiredPeriodsCount * resolution / (24 * 60);
	}

	return daysCount * 24 * 60 * 60;
};
if(typeof module !== 'undefined' && module && module.exports) {
	module.exports = {
		UDFCompatibleDatafeed: Datafeeds.UDFCompatibleDatafeed,
	};
}