mui.init({
	swipeBack: false,
	drag: false
});
document.getElementsByClassName('mui-inner-wrap')[0].addEventListener('drag', function(event) {
	event.stopPropagation();
});
 //侧滑容器父节点
var offCanvasWrapper = mui('#offCanvasWrapper');
 //主界面容器
var offCanvasInner = offCanvasWrapper[0].querySelector('.mui-inner-wrap');
 //菜单容器
var offCanvasSide = document.getElementById("offCanvasSide");
 //Android暂不支持整体移动动画
if (!mui.os.android) {
	//document.getElementById("move-togger").classList.remove('mui-hidden');
	var spans = document.querySelectorAll('.android-only');
	for (var i = 0, len = spans.length; i < len; i++) {
		spans[i].style.display = "none";
	}
}
 //移动效果是否为整体移动
var moveTogether = true;
offCanvasInner.insertBefore(offCanvasSide, offCanvasInner.firstElementChild);
document.getElementById('offCanvasHide').addEventListener('tap', function() {
	offCanvasWrapper.offCanvas('close');
});
 //侧滑容器的class列表，增加.mui-slide-in即可实现菜单移动、主界面不动的效果；
//var classList = offCanvasWrapper[0].classList;
 //主界面和侧滑菜单界面均支持区域滚动；
mui('#offCanvasSideScroll').scroll();
mui('#offCanvasContentScroll').scroll();
mui('#searchscroll').scroll();
mui('#selectscroll').scroll();
mui('#exchangescroll').scroll();

mui("#favoritescroll").scroll();
mui("#buyscroll").scroll();
mui("#sellscroll").scroll();
mui('#DepositContentscroll').scroll();
mui("#widthdrawContentscroll").scroll();
mui("#historyscroll").scroll();

//头部信息显示
$(function(){
		var userinfo = localStorage.getItem('username');
		console.log(userinfo);
		if(userinfo == undefined || userinfo == null){
			$('.username').html('未登录');
			document.getElementById('userinfo').addEventListener('tap',function(){
				mui.openWindow({
					url:'../login.html'
				});
			});
		}else{
			$('.username').html("个人中心");
			document.getElementById('userinfo').addEventListener('tap',function(){
				mui.openWindow({
					url:'../my.html'
				});
			});
		}
	})
//验证登录状态
function loginstatus(data){
	if(data.errors != undefined){
		if(data.errors[0].code == 4000){
			mui.openWindow({
				url:'../login.html'
			});
			localStorage.removeItem('username');
		}
	}
	if(localStorage.getItem('username') == undefined){
		mui.openWindow({
			url:'../login.html'
		});
		localStorage.removeItem('username');
	}
}
//多语言相关
	$("#gochinese").on('tap',function(){
		localStorage.setItem("lan","cn")
		window.location.reload();
	});
	$("#goenglish").on("tap",function(){
		localStorage.setItem("lan","en")
		window.location.reload();
	})
	$('[set-lan]').each(function(){
    var me = $(this);
    var a = me.attr('set-lan').split(':');
    var p = a[0];   //文字放置位置
    var m = a[1];   //文字的标识

    //用户选择语言后保存在cookie中，这里读取cookie中的语言版本
    var lan = localStorage.getItem("lan");

    //选取语言文字
    switch(lan){
        case 'cn':
            var t = cn[m];  //这里cn[m]中的cn是上面定义的json字符串的变量名，m是json中的键，用此方式读取到json中的值
            break;
        case 'en':
            var t = en[m];
            break;
        default:
            var t = cn[m];
    }

    //如果所选语言的json中没有此内容就选取其他语言显示
    if(t==undefined) t = cn[m];
    if(t==undefined) t = en[m];

    if(t==undefined) return true;   //如果还是没有就跳出

    //文字放置位置有（html,val等，可以自己添加）
    switch(p){
        case 'html':
            me.html(t);
            break;
        case 'val':
        case 'value':
            me.val(t);
            break;
        default:
            me.html(t);
    }

});
function get_lan(m)
{
    //获取文字
    var lan = localStorage.getItem("lan");    //语言版本
    //选取语言文字
    switch(lan){
        case 'cn':
            var t = cn[m];
            break;
        case 'en':
            var t = en[m];
            break;
        default:
            var t = cn[m];
    }

    //如果所选语言的json中没有此内容就选取其他语言显示
    if(t==undefined) t = cn[m];
    if(t==undefined) t = en[m];

    if(t==undefined) t = m; //如果还是没有就返回他的标识

    return t;
}
///导航链接
document.getElementById('logo').addEventListener('tap',function(){
	mui.openWindow({
		url:'../index.html'
	});
});
document.getElementById('gohome').addEventListener('tap',function(){
	mui.openWindow({
		url:'../index.html'
	});
});
document.getElementById('goexchange').addEventListener('tap',function(){
	mui.openWindow({
		url:'test.html',
		extras: {
			currency:'ETH-BTC',
		}
	});
});
document.getElementById('goassetRortfolio').addEventListener('tap',function(){
	mui.openWindow({
		url:'../AssetDepositWithdrawal.html',
		extras: {
			type:'wallet',
		}
	});
});
document.getElementById('goDepositRecords').addEventListener('tap',function(){
	mui.openWindow({
		url:'../AssetDepositWithdrawal.html',
		extras: {
			type:'deposit',
		}
	});
});
document.getElementById('goWithdrawalRecords').addEventListener('tap',function(){
	mui.openWindow({
		url:'../AssetDepositWithdrawal.html',
		extras: {
			type:'withdraw',
		}
	});
});
document.getElementById('goOpenOrders').addEventListener('tap',function(){
	mui.openWindow({
		url:'../ordersandtrade.html'
	});
});