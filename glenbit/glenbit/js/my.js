
document.getElementById('goemailadress').addEventListener('tap',function(){
	mui.openWindow({
		url:'Resetemail.html'
	});
})
document.getElementById('gologinpassword').addEventListener('tap',function(){
	mui.openWindow({
		url:'Resetloginpassword.html'
	});
})
document.getElementById('goIDverification').addEventListener('tap',function(){
// 	mui.openWindow({
// 		url:'IDverification.html'
// 	});
})
document.getElementById('goCreate').addEventListener('tap',function(){
	mui.openWindow({
		url:'Create.html'
	});
});
$(function(){
	loadingajax();
	$("#gocancel").on('tap',function(){
		$('.canceltips').fadeIn();
	});
	$(".cancelbtn").on('tap',function(){
		$('.canceltips').fadeOut();
	});
	$(".confirmbtn").on('tap',function(){
		$('.canceltips').fadeOut();
		$.ajax({
			url:'https://rest.glenbit.com/account/logout',
			type:'POST',
			dataType:'json',
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			success:function(data){
				console.log(data);
				mui.openWindow({
					url:'login.html'
				});
				localStorage.removeItem("username");
			}
		})
	});
})
function loadingajax(){
	$.ajax({
		url: 'https://rest.glenbit.com/account/profile',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success: function (data) {
			console.log(data);
			loginstatus(data);
			$(".myusername").html(data.email);
			var date = new Date(parseInt(data.recentLogins[0].createdAt));		
			var userip = data.recentLogins[0].ip.split(',');
			$(".lastsignip").html(userip[0]);
			if(data.isSmsVerified == false){
				//$('.smsauth').html('未启用');
				$('.smsauth').html(get_lan('Disabled'));
				document.getElementById('goDisableSMS').addEventListener('tap',function(){
					mui.openWindow({
						url:'DisableSMSAuthenticationConfirmEmailGetcode2.html'
					});
				})
			}else if(data.isSmsVerified == true){
				//$('.smsauth').html('已启用');
				$('.smsauth').html(get_lan('Enabled'));
			}
			if(data.isTwoStepEnabled == false){
				//$('.googleauth').html('未启用');
				$('.googleauth').html(get_lan('Disabled'));
				document.getElementById('goEnableGoole').addEventListener('tap',function(){
					mui.openWindow({
						url:'googleVerification.html'
					});
				});
			}else if(data.isTwoStepEnabled == true){
				$('.googleauth').html(get_lan('Enabled'));
				document.getElementById('goEnableGoole').addEventListener('tap',function(){
					$('.googleVerification').fadeIn();
						console.log('1')
					
				})
			}
			var iphtml = '';
			for(var i = 0 ; i < data.recentLogins.length; i++){
				var now = new Date(parseInt(data.recentLogins[i].createdAt));
				y = now.getFullYear();
				m = ("0" + (now.getMonth() + 1)).slice(-2);
				d = ("0" + now.getDate()).slice(-2);
				var userip = data.recentLogins[i].ip.split(',');
				iphtml+='<li>'+
							'<div class="w75 h132 borderbDB bgcolorff ">'+
								'<span class="mt032 w19 lh3 fl f28  color10 ml036">'+
									'<span class="f28 db">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</span>'+
								'</span>'+
								'<span class="mt044 fr color10  f28 mr036">'+
									'<span class="f28">'+userip[0]+'</span>'+ 
								'</span>'+
							'</div>'+
						'</li>'
			}
			$('.loginiplist').append(iphtml);
			getdate(date);
		}
	});
}
$('.googleclose').on('tap',function(){
	$('.googleVerification').fadeOut();
});
$('.googlesubmit').on('tap',function(){
	var googletoken = $('.googletoken').val();
	if(googletoken == ''){
		mui.toast("验证码不能为空")
	}else{
		$.ajax({
			url:'https://rest.glenbit.com/account/disable_two_step',
			type:'POST',
			dataType:'json',
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			data:{
				token: googletoken
			},
			success:function(data){
				console.log(data);
				loadingajax();
				$('.googleVerification').fadeOut();
			}
		})
	}		
});
function getdate(date) {
		var now = new Date(date),
		y = now.getFullYear(),
		m = ("0" + (now.getMonth() + 1)).slice(-2),
		d = ("0" + now.getDate()).slice(-2);
		$('.lastsigntime').html(y + "-" + m + "-" + d + " " + now.toTimeString().substr(0, 8));
	//return y + "-" + m + "-" + d + " " + now.toTimeString().substr(0, 8);
}



