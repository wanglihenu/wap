
	document.getElementById('gophonelogin').addEventListener('tap',function(){
		document.getElementById('emailloginbox').style.display = 'none';
		document.getElementById('phoneloginbox').style.display = 'block';
	});
	document.getElementById('goemaillogin').addEventListener('tap',function(){
		document.getElementById('emailloginbox').style.display = 'block';
		document.getElementById('phoneloginbox').style.display = 'none';
	});
	document.getElementById('goforgotpassword').addEventListener('tap',function(){
		mui.openWindow({
			url:'retrievepassword.html'
		});
	});
	document.getElementById('gosingnup').addEventListener('tap',function(){
		mui.openWindow({
			url:'signup.html'
		});
	});
	document.getElementById('areacode').addEventListener('tap',function(){
		document.getElementById('areatips').style.left = '0';
	});
	document.getElementById('areaback').addEventListener('tap',function(){
		document.getElementById('areatips').style.left = '100%';
	});

	//
	$(function(){
		$('.loginemail').attr('placeholder',get_lan("EMAIL_PLZ"))
		$('.loginpassword').attr('placeholder',get_lan("PASSWORD_PLZ"))
		$('.phoneloginemail').attr('placeholder',get_lan("Eenter_Phone_Number"))
		$('.phoneloginpassword').attr('placeholder',get_lan("PASSWORD_PLZ"))
		$('.googletoken').attr('placeholder',get_lan("Enter_verification_Code"))
		$.ajax({
	// 获取id，challenge，success（是否启用failback）
			url: " https://rest.glenbit.com/gt/register?t=" + (new Date()).getTime(), // 加随机数防止缓存
			type: "get",
			dataType: "json",
			success: function (data) {
				console.log(data);
				// 使用initGeetest接口
				// 参数1：配置参数
				// 参数2：回调，回调的第一个参数验证码对象，之后可以使用它做appendTo之类的事件
				initGeetest({
					gt: data.gt,
					challenge: data.challenge,
					offline: !data.success, // 表示用户后台检测极验服务器是否宕机
					new_captcha: data.new_captcha, // 用于宕机时表示是新验证码的宕机
					product: "bind", // 产品形式，包括：float，popup
					width: "300px",
					https: true
				}, handlerPopup);
			}
		});
		$.ajax({
			url: "../json/countries.json", // 加随机数防止缓存
			type: "get",
			dataType: "json",
			success: function (data) {
				var html = '';
				var regex1 = /\((\d+)\)/;   // () 小括号
				for(var i = 0 ; i < data.length;i++){
					html+="<li class='arealist'>"+data[i].countryCode.displayValue+"<span class='arealistcontent' style='display:none'>"+data[i].countryCode.rawValue+"</span></li>";
				}
				document.getElementById("areaul").innerHTML = html;
				$(".arealist").each(function(){
					
					$(this).on('tap',function(){
						var str = $(this).html();
						var strtwo = str.substring(str.indexOf("(")+1,str.indexOf(")"))
						$(".areanumber").html(strtwo);
						$(".areaselect").html($(this).find('.arealistcontent').html());
						document.getElementById('areatips').style.left = '100%';
						//$(".SecretKey").fadeIn()
					})
				})
			}	
		})	
		})
		
	   // 代码详细说明
	  var loginemail = '';
	  var loginpassword = '';
	var handlerPopup = function (captchaObj) {
	    // 注册提交按钮事件，比如在登陆页面的登陆按钮
	     captchaObj.onReady(function () {
	            $("#wait").hide();
	        }).onSuccess(function () {
	            var result = captchaObj.getValidate();
	    		console.log(result);
				console.log(loginemail +','+ loginpassword)
	            if (!result) {
	                return mui.toast('请完成验证');
	            }
	            $.ajax({
	                url: 'https://rest.glenbit.com/account/login',
	                type: 'POST',
	                dataType: 'json',
					xhrFields: {
						withCredentials: true
					},
					crossDomain: true,
	                data: {
	                    email: loginemail,
	                    password: loginpassword,
	                    geetest_challenge: result.geetest_challenge,
	                    geetest_validate: result.geetest_validate,
	                    geetest_seccode: result.geetest_seccode,
	                },
	                success: function (data,status,xhr) {
						
	    				console.log(data.errors);
						if(data.errors != undefined && data.errors[0].code == '4006'){
							$('.googleVerification').fadeIn();
							
							$('.googlesubmit').on('tap',function(){
								
								var googletoken = $('.googletoken').val();
								if(googletoken == ''){
									alert("验证码不能为空")
								}else{
									$.ajax({
										url:'https://rest.glenbit.com/account/login_ga',
										type:'POST',
										dataType:'json',
										xhrFields: {
											withCredentials: true
										},
										crossDomain: true,
										data:{
											email: loginemail ,
											password: loginpassword,
											token: googletoken
										},
										success:function(data){
											$('.googleVerification').fadeOut();
											console.log(data);
											mui.openWindow({
												url:'AssetDepositWithdrawal.html'
											})
											localStorage.setItem('username',data.email);
											localStorage.setItem('userarea',$('.areaselect').html())
										}
									})
								}		
							})
						}else if(data.errors == undefined){
							console.log(data);
							mui.openWindow({
								url:'AssetDepositWithdrawal.html'
							})
							localStorage.setItem('username',data.email);
							localStorage.setItem('userarea',$('.areaselect').html())
						}
						


// 						mui.openWindow({
// 							url:'../index/index.html'
// 						});
	                }
	            });
	        });
	        document.getElementById('loginbtn').addEventListener('tap',function () {
	            // 调用之前先通过前端表单校验
				
				loginemail = $(".loginemail").val();
				loginpassword = $(".loginpassword").val();
				if(loginemail == ''){
					mui.toast(get_lan("EMAIL_PLZ"));
					//alert("邮箱不能为空");
					return
				}else if(loginemail.indexOf('@') == -1){
					mui.toast(get_lan("EMAIL_INVALID"));
					
					//alert("邮箱不能为空");
					return
				}
				else if(loginpassword == ''){
					//alert("密码不能为空");
					mui.toast(get_lan("PASSWORD_PLZ"));
					return
				}else if(loginpassword.length < 6){
					mui.toast(get_lan("PASSWORD_INVALID"));
					
					return
				}
				else{
					captchaObj.verify();
				}
	            
	        });
			$('.googleclose').on('tap',function(){
				$('.googleVerification').fadeOut();
			});
			document.getElementById('phoneloginbtn').addEventListener('tap',function () {
			    // 调用之前先通过前端表单校验
				loginemail = $('.areaselect').html() + $('.phoneloginemail').val();
				var loginphone = $('.phoneloginemail').val();
				console.log(loginemail)
				loginpassword = $(".phoneloginpassword").val();
				if(loginphone == ''){
					//alert("手机号不能为空");
					mui.toast(get_lan("Eenter_Phone_Number"));
					return
				}else if(loginpassword == ''){
					//alert("密码不能为空");
					mui.toast(get_lan("PASSWORD_PLZ"));
					return
				}else if(loginpassword.length < 6){
					mui.toast(get_lan("PASSWORD_INVALID"));
					return
				}
				else{
					captchaObj.verify();
				}
			    
			});
	        // 更多接口说明请参见：http://docs.geetest.com/install/client/web-front/
	    };