$(function(){
	var ratearr = [];
	var withdrawcoin = '';
	var walletlist = '';
	var mywalletsrr = [];
	var isTwoStepEnabled = '';
	$.ajax({
		url: 'https://rest.glenbit.com/account/profile',
		type: 'POST',
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success: function (data) {
			loginstatus(data)
			console.log(data);
			isTwoStepEnabled = data.isTwoStepEnabled;
		}
	})
	$.ajax({
		url:'https://rest.glenbit.com/coins',
		type:'GET',
		dataType:'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success:function(data){
			 function sortId(a,b){  
			   return a.sortOrder-b.sortOrder  
			}
			data.sort(sortId);
			console.log(data)
			for(var i =0; i< data.length;i++){
				mywalletsrr.push(data[i]._id);
				walletlist += '<li class="depositRecord_contents walletlists">'+
					'<div class="w75 h132 borderbDB bgcolorff depositRecord_rightimgparent1">'+
						'<img class="withdrawlogo" src = "../images/selected_market-off@2x.png" />'+
						'<span class="t-l pt028 fl f28  color10 pl036 cionnamefather">'+
							'<span class="f36 lh4 coinname">'+data[i]._id +'</span>'+
							'<span class="f36 lh4">('+data[i].name+')</span>'+
							'<div class="f24 color35 h018 f18 lh4">0.000000</div>'+
						'</span>'+
						'<span class="w10 t-c mt044 fr color10  f28 pr036 depositRecord_rightimgparent0">'+
							'<span class="f28 depositRecord_rightimgparent"> '+
								'<span class="fr depositRecord_rightimg ">'+
									'<span class="isshow down">'+	
									'</span>'+
								'</span>'+
							'</span>'+ 
						'</span>'+
						'<span class="t-r fr color10  pt028 f28   breaked">'+
							'<span class="f36 lh4 ">0.000000</span>'+
							'<div class="f24 color35 h018 f18 lh4">0.000000</div> '+ 
						'</span>'+
					'</div>'+ 
					'<div class="Assethidden">'+
						'<ul>'+
							'<li class="fl Assethiddenright Depositpop">'+
								'<img src="../images/wallet/icon_wallet_deposit.png" alt="" />'+
							'</li>'+
							'<li class="fl Assethiddenright Withdrawpop">'+
								'<img src="../images/wallet/icon_wallet_withdraw.png" alt="" />'+
							'</li>'+
							'<li class="fl del" id="id0" >'+
								'<img src="../images/wallet/icon_wallet_trade.png" alt="" />'+
							'</li>'+
						'</ul>'+
					'</div>'+
				'</li>'
				$("#Deposit_selected").append("<option value='"+data[i]._id+"'>"+data[i]._id+"</option>");
				$("#withdraw_selected").append("<option value='"+data[i]._id+"'>"+data[i]._id+"</option>");
				$("#chongzhi_selected").append("<option value='"+data[i]._id+"'>"+data[i]._id+"</option>");
			}
			$("#Listofassets").html(walletlist);
			$("#Deposit_selected").on('change',function(){ 
				//获取到搜索的value值提交
				var Deposit_search = $("#Deposit_search").val()
				console.log($(this).val())
				for(var i =0; i< mywalletsrr.length;i++){
					if($(this).val() == 'ALL' || $(this).val() == '全部'){
						$(".mywalletlist").find(".depositRecord_contents").css('display','block')
					}
					else if($(this).val() == mywalletsrr[i]){
						//console.log(i)
						$(".mywalletlist").find(".depositRecord_contents").css('display','none')
						$(".mywalletlist").find(".depositRecord_contents")[i].style.display = 'block';
						
					}
				}
				//获取到select的选中值
			})
			$.ajax({
				url:'https://rest.glenbit.com/wallet/balance',
				type:'POST',
				dataType:'json',
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success:function(res){
					console.log(res)
					loginstatus(res)
					var withdrawlist = '';
					for(var j = 0; j < res.length;j++){
						for(var i =0;i< data.length;i++){
								if(res[j].coin == data[i]._id){	
									withdrawlist = '<div class="w75 h132 borderbDB bgcolorff depositRecord_rightimgparent1">'+
										'<img class="withdrawlogo" src = "../images/selected_market-off@2x.png" />'+
										'<span class="t-l pt028 fl f28  color10 pl036 cionnamefather">'+
											'<span class="f36 lh4 coinname">'+res[j].coin +'</span>'+
											'<span class="f36 lh4">('+data[i].name+')</span>'+
											'<div class="f24 color35 h018 f18 lh4">'+(Number(res[j].free) + Number(res[j].locked)).toFixed(6)+'</div>'+
										'</span>'+
										'<span class="w10 t-c mt044 fr color10  f28 pr036 depositRecord_rightimgparent0">'+
											'<span class="f28 depositRecord_rightimgparent"> '+
												'<span class="fr depositRecord_rightimg ">'+
													'<span class="isshow down">'+
														
													'</span> '+
												'</span>'+
											'</span>'+ 
										'</span>'+
										'<span class=" t-r fr color10  pt028 f28   breaked">'+
											'<span class="f36 lh4 ">'+(res[j].free).toFixed(6)+'</span>'+
											'<div class="f24 color35 h018 f18 lh4">'+(res[j].locked).toFixed(6)+'</div> '+ 
										'</span>'+
									'</div>'+ 
									'<div class="Assethidden">'+
										'<ul>'+
											'<li class="fl Assethiddenright Depositpop">'+
												'<img src="../images/wallet/icon_wallet_deposit.png" alt="" />'+
											'</li>'+
											'<li class="fl Assethiddenright Withdrawpop">'+
												'<img src="../images/wallet/icon_wallet_withdraw.png" alt="" />'+
											'</li>'+
											'<li class="fl del" id="id0" >'+
												'<img src="../images/wallet/icon_wallet_trade.png" alt="" />'+
											'</li>'+
										'</ul>'+
									'</div>'
								//console.log(data[i]._id)
								//console.log(i)
								//console.log(withdrawlist)
								
								//console.log(document.getElementsByClassName("depositRecord_contents")[i].innerHTML)
								//document.getElementsByClassName("depositRecord_contents")[i].innerHTML = '';
								document.getElementsByClassName("walletlists")[i].innerHTML = withdrawlist
								//console.log(document.getElementsByClassName("walletlists")[i].innerHTML)	
								
							}
							withdrawlist = '';	
						}
					}
							$("#Listofassets .isshow").each(function(){
								$(this).on('tap',function(){ 
									var $pth = $(this).parent(".depositRecord_rightimg").parent(".depositRecord_rightimgparent").parent(".depositRecord_rightimgparent0").parent(".depositRecord_rightimgparent1").parent(".depositRecord_contents"); 
									if($(this).hasClass("up")){
										$(this).addClass("down")
										$(this).removeClass("up") 
										$pth.find(".Assethidden").fadeOut();
										$pth.find(".DepositTXID_isshow").fadeOut(); 
									}else if($(this).hasClass("down")){ 
										$(this).removeClass("down");
										$(this).addClass("up");
										$pth.find(".Assethidden").fadeIn();
										$pth.find(".DepositTXID_isshow").fadeIn(); 
									} 
								})
							})
							$(".Depositpop").each(function(){
								$(this).on('tap',function(){
									//coinname  depositRecord_rightimgparent1
									var coinname = $(this).parent("ul").parent(".Assethidden").siblings(".depositRecord_rightimgparent1").children('.cionnamefather').children('.coinname').html(); 
									//console.log($(this).parent("ul").parent(".Assethidden").siblings(".depositRecord_rightimgparent1").children('.cionnamefather').children('.coinname').html())
									console.log(coinname)
									$(".depositcoin").html(coinname)
									for(var i = 0 ; i< data.length; i++){
										if(coinname == data[i]._id){
											if(data[i].canDeposit == true){
												$.ajax({
													url:'https://rest.glenbit.com/wallet/address',
													type:'POST',
													dataType:'json',
													xhrFields: {
														withCredentials: true
													},
													crossDomain: true,
													data:{
														coin: coinname
													},
													success:function(res){
														loginstatus(res)
														console.log(res);
														if(res.length > 5){
															var cionadress = '';
															var selectindex = res.split(' ');
															
															if(selectindex.length == 1){
																cionadress = selectindex[0].substring(1,selectindex[0].length - 1);
															}else if(selectindex.length > 1){
																cionadress = selectindex[0].substring(1,selectindex[0].length);	
															}
															//var cionadress = data.substring(selectindex+1,data.length - 1);
															//console.log(wwww +','+selectpush)
															elText = cionadress
															//绘制二维码 这里因为是传的值，所以有值的时候要检查一下，每次绘制的是不是值有变化
															qrcode.makeCode(elText);
															$("#DepositAddress").html(cionadress)
															$(".Deposit").fadeIn()
														}
														
													}
												});
											}else{
												mui.toast(get_lan("Pauserecharge"))
											}
									
									//Deposit每次都会有值得改变
									//二维码的内容
										}
									}
								})
							})
							$(".Withdrawpop").each(function(){
								$(this).on('tap',function(){
									console.log(isTwoStepEnabled)
									if(isTwoStepEnabled == false){
										mui.toast(get_lan('OpenGoogleSecondCertification'))
										return;
									}
									withdrawcoin = $(this).parent("ul").parent(".Assethidden").siblings(".depositRecord_rightimgparent1").children('.cionnamefather').children('.coinname').html(); 
									//console.log($(this).parent("ul").parent(".Assethidden").siblings(".depositRecord_rightimgparent1").children('.cionnamefather').children('.coinname').html())
									$('.withdrawname').html(withdrawcoin)
									for(var i = 0 ; i< data.length; i++){
										if(withdrawcoin == data[i]._id){
											if(data[i].canWithdraw == true){
												$(".Withdraw").fadeIn()
												$.ajax({
													url:'https://rest.glenbit.com/wallet/try_withdraw',
													type:'POST',
													dataType:'json',
													data:{
														coin: withdrawcoin
													},
													xhrFields: {
														withCredentials: true
													},
													crossDomain: true,
													success:function(res){
														console.log(res)
														loginstatus(res)
														$('.withdrawnumber').html(res.minWithdraw)
														$('.withdrawAvailable').html(res.available);
														$('.withdrawFee').html(res.fee);
													}
												});
											}else{
												mui.toast(get_lan("Suspensionwithdrawal"))
											}
										}
									}
									
								})
							})
							$(".buttonsubmit").on('tap',function(){
								var sendData = {}
								var WithdrawAddress = $("#Withdrawadress").val();
								var WithdrawAmount = $("#WithdrawAmount").val();
								var Withdrawcode = $("#Withdrawcode").val();
								sendData["WithdrawAddress"] = WithdrawAddress;
								sendData["WithdrawAmount"] = WithdrawAmount;
								sendData["Withdrawcode"] = Withdrawcode;
								console.log(sendData)
								//提交数据
								if(WithdrawAddress!='' && WithdrawAmount!='' && Withdrawcode!=''){
									//提交完数据弹框要消失
									$.ajax({
										url:'https://rest.glenbit.com/wallet/withdraw',
										type:'POST',
										dataType:'json',
										data:{
											address: WithdrawAddress,
											coin: withdrawcoin,
											amount: WithdrawAmount,
											token: Withdrawcode
										},
										xhrFields: {
											withCredentials: true
										},
										crossDomain: true,
										success:function(data){
											console.log(data);
											loginstatus(data)
											$(".findout").fadeOut()
										}
									});
									
								}else{
									//处理数据异常情况
								}
							})
				}			
			})
		}
		
	})
	
	$.ajax({
		url:'https://rest.glenbit.com/market_prices',
		type:'GET',
		dataType:'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success:function(data){
			console.log(data);
			var rateone = '';
			var ratetwo = '';
			var mybalance = '';
			var rateto = [];
			for(var i = 0;i < data.length;i++){
				ratearr.push(data[i]);
				if(data[i].from == 'BTC' && data[i].to == 'USDT'){
					rateone = data[i].price;
				}
				if(data[i].from == 'USDT' && data[i].to == 'USD'){
					ratetwo = data[i].price;
				}	
			}
			$('.btcusdrate').html((Number(rateone) * Number(ratetwo)).toFixed(2));
			$.ajax({
				url:'https://rest.glenbit.com/wallet/balance',
				type:'POST',
				dataType:'json',
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success:function(data){
					console.log(data);
					loginstatus(data)
				//	console.log(ratearr);
					var Variouscoin = '';
					var Variouscointwo = 0;
					for(var i = 0 ; i < data.length; i++){
						
						//console.log(data[i]);
 						var totalassets = Number(data[i].free) +  Number(data[i].locked);
// 						//console.log(totalassets)
						for(var j = 0; j < ratearr.length; j++){
							//console.log(ratearr[j])
							if(data[i].coin == ratearr[j].from){
								//console.log(ratearr[j].to)
								if(ratearr[j].to == 'USD'){
								Variouscoin = totalassets * ratearr[j].price;
								}else if(ratearr[j].to == 'USDT'){
								Variouscoin = totalassets * ratearr[j].price * ratetwo;
								}else if(ratearr[j].to == 'BTC'){
								Variouscoin = totalassets * ratearr[j].price * rateone *  ratetwo;
								}
							}
						}	
						Variouscointwo += Number(Variouscoin);
					}
					
					$("#TotalAssetUsd").html(Variouscointwo.toFixed(2) + ' USD')
					$("#TotalAssetUsdrate").html((Variouscointwo / (rateone *  ratetwo)).toFixed(8))
					
					 
				}
			});
		}
	});
	var chongzhiarr =[];
	$.ajax({
		url:'https://rest.glenbit.com/wallet/deposit_history',
		type:'POST',
		dataType:'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success:function(data){
			var deposit = '<';
			var depositwait = '';
			console.log(data)
			loginstatus(data)
			var state = '';
			for(var i =0;i< data.length;i++){
				chongzhiarr.push(data[i].coin)
				var now = new Date(parseInt(data[i].createdAt));
				y = now.getFullYear();
				m = ("0" + (now.getMonth() + 1)).slice(-2);
				d = ("0" + now.getDate()).slice(-2);
				if(data[i].txid == '' && data[i].updateAt == ''){
					state = '审核中';
				}else if(data[i].txid == ''){
					state = '处理中';
				}else if(data[i].txid != ''){
					state = '已完成';
				}
				if(data[i].updatedAt == ''){
					console.log(111);
					depositwait += ' <div class="depositRecord_contents">'+
								'<div class="w75 h132 borderbDB bgcolorff depositRecord_rightimgparent1">'+
									'<span class="fl pl036 mt040">'+data[i].coin+'</span>'+
									'<span class="t-l mt032 w25 lh3 fl f28  color10 pl036">'+
										'<span class="f28 db">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</span>'+
									'</span>'+
									'<span class="w25 t-c mt044 fr color10  f28 pr036 depositRecord_rightimgparent0">'+
										'<span class="f28 depositRecord_rightimgparent">'+
											'<span class="fl pl036">'+state+'</span>'+
											'<span class="fr depositRecord_rightimg ">'+
												'<span class="isshowtwo down">'+
													
												'</span>'+
											'</span>'+
										'</span> '+
									'</span>'+
									'<span class="t-r fr color10  mt044 f28   breaked">'+
										'<span class="f36 color00A lh4">'+data[i].amount+'</span>'+
									'</span>'+
								'</div>'+
								'<div class="DepositTXID_isshow">'+
									'<div class="AccessId mb4">'+
										'<p class="f30 color10" >'+get_lan("DepositAddress")+'</p>'+
										'<p class="f32 color10 AccessIdcontent  breaked ">0xdfebbcae754df178e0dfec83e051f1c6b81b2fda</p>'+
									'</div>'+
									'<div class="AccessId">'+
										'<p class="f30 color10">TXID</p>'+
										'<p class="f32 color10 AccessIdcontent  breaked ">'+data[i].txid+'</p>'+
									'</div>'+
								'</div>'+
							'</div>'
				}
				else{
					deposit += ' <div class="depositRecord_contents">'+
								'<div class="w75 h132 borderbDB bgcolorff depositRecord_rightimgparent1">'+
									'<span class="fl pl036 mt040">'+data[i].coin+'</span>'+
									'<span class="t-l mt032 w25 lh3 fl f28  color10 pl036">'+
										'<span class="f28 db">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</span>'+
									'</span>'+
									'<span class="w25 t-c mt044 fr color10  f28 pr036 depositRecord_rightimgparent0">'+
										'<span class="f28 depositRecord_rightimgparent">'+
											'<span class="fl pl036">'+state+'</span>'+
											'<span class="fr depositRecord_rightimg ">'+
												'<span class="isshowtwo down">'+
													
												'</span>'+
											'</span>'+
										'</span> '+
									'</span>'+
									'<span class="t-r fr color10  mt044 f28   breaked">'+
										'<span class="f36 color00A lh4">'+data[i].amount+'</span>'+
									'</span>'+
								'</div>'+
								'<div class="DepositTXID_isshow">'+
									'<div class="AccessId mb4">'+
										'<p class="f30 color10" >'+get_lan("DepositAddress")+'</p>'+
										'<p class="f32 color10 AccessIdcontent  breaked ">0xdfebbcae754df178e0dfec83e051f1c6b81b2fda</p>'+
									'</div>'+
									'<div class="AccessId">'+
										'<p class="f30 color10">TXID</p>'+
										'<p class="f32 color10 AccessIdcontent  breaked ">'+data[i].txid+'</p>'+
									'</div>'+
								'</div>'+
							'</div>'
				}
			}
			$('#Pendingdeposit').html(depositwait);
			$('.deposithistory').html(deposit);
			
			$(".isshowtwo").each(function(){
				$(this).on('tap',function(){ 
					var $pth = $(this).parent(".depositRecord_rightimg").parent(".depositRecord_rightimgparent").parent(".depositRecord_rightimgparent0").parent(".depositRecord_rightimgparent1").parent(".depositRecord_contents"); 
					if($(this).hasClass("up")){
						$(this).addClass("down")
						$(this).removeClass("up") 
						$pth.find(".Assethidden").fadeOut();
						$pth.find(".DepositTXID_isshow").fadeOut(); 
					}else if($(this).hasClass("down")){ 
						$(this).removeClass("down");
						$(this).addClass("up");
						$pth.find(".Assethidden").fadeIn();
						$pth.find(".DepositTXID_isshow").fadeIn(); 
					} 
				})
			})
		}
	});
	$("#chongzhi_selected").on('change',function(){ 
		//获取到搜索的value值提交
		$(".deposithistory").find(".depositRecord_contents").css('display','none')
		console.log($(this).val())
		for(var i =0; i< chongzhiarr.length;i++){
			if($(this).val() == 'ALL' || $(this).val() == '全部'){
				$(".deposithistory").find(".depositRecord_contents").css('display','block')
			}
			else if($(this).val() == chongzhiarr[i]){
				console.log($(".deposithistory").length)
				
				$(".deposithistory").find(".depositRecord_contents")[i].style.display = 'block';
				
			}
		}
		//获取到select的选中值
	})
	var withdrawarr = [];
	$.ajax({
		url:'https://rest.glenbit.com/wallet/withdraw_history',
		type:'POST',
		dataType:'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success:function(data){
			console.log(data)
			loginstatus(data)
			 function sortId(a,b){  
			   return b.updatedAt-a.updatedAt  
			}
			data.sort(sortId);
			var widtdrawhistory = '';
			var state = '';
			for(var i = 0 ; i < data.length; i++){
				withdrawarr.push(data[i].coin);
				var now = new Date(parseInt(data[i].createdAt));
				y = now.getFullYear();
				m = ("0" + (now.getMonth() + 1)).slice(-2);
				d = ("0" + now.getDate()).slice(-2);
				if(data[i].txid == '' && data[i].updateAt == ''){
					state = '审核中';
				}else if(data[i].txid == ''){
					state = '处理中';
				}else if(data[i].txid != ''){
					state = '已完成';
				}
				widtdrawhistory += '<div class="depositRecord_contents">'+
						'<div class="w75 h132 borderbDB bgcolorff depositRecord_rightimgparent1">'+
							'<span class="fl pl036 mt040">'+data[i].coin+'</span>'+
							'<span class="t-l mt032 w25 lh3 fl f28  color10 pl036">'+
								'<span class="f28 db">'+y + '-' + m + '-' + d + ' ' + now.toTimeString().substr(0, 8)+'</span>'+
							'</span>'+
							'<span class="w25 t-c mt044 fr color10  f28 pr036 depositRecord_rightimgparent0">'+
								'<span class="f28 depositRecord_rightimgparent">'+
									'<span class="fl pl036">'+state+'</span>'+
									'<span class="fr depositRecord_rightimg ">'+
										'<span class="isshowthree down">'+
											
										'</span> '+
									'</span>'+
								'</span>'+ 
							'</span>'+
							'<span class="w12 t-r fr color10  mt044 f28   breaked">'+
								'<span class="f36 color00A lh4">'+data[i].amount+'</span>'+
							'</span>'+
						'</div>'+
						'<div class="DepositTXID_isshow">'+
						'<div class="AccessId mb4">'+
							'<p class="f30 color10">'+get_lan("WithdrawAddress")+'</p>'+
							'<p class="f32 color10 AccessIdcontent  breaked ">'+data[i].address+'</p>'+
						'</div>'+
						'<div class="AccessId">'+
							'<p class="f30 color10">TXID</p>'+
							'<p class="f32 color10 AccessIdcontent  breaked ">'+data[i].txid+'</p>'+
						'</div>'+
					'</div>'+
				'</div>' 
			}
			
			$('.withdrawhistory').html(widtdrawhistory);
			$(".isshowthree").each(function(){
				$(this).on('tap',function(){ 
					var $pth = $(this).parent(".depositRecord_rightimg").parent(".depositRecord_rightimgparent").parent(".depositRecord_rightimgparent0").parent(".depositRecord_rightimgparent1").parent(".depositRecord_contents"); 
					if($(this).hasClass("up")){
						$(this).addClass("down")
						$(this).removeClass("up") 
						$pth.find(".Assethidden").fadeOut();
						$pth.find(".DepositTXID_isshow").fadeOut(); 
					}else if($(this).hasClass("down")){ 
						$(this).removeClass("down");
						$(this).addClass("up");
						$pth.find(".Assethidden").fadeIn();
						$pth.find(".DepositTXID_isshow").fadeIn(); 
					} 
				})
			})
		}
	});
	$("#withdraw_selected").on('change',function(){ 
		//获取到搜索的value值提交
		$(".withdrawhistory").find(".depositRecord_contents").css('display','none')
		for(var i =0; i< withdrawarr.length;i++){
			if($(this).val() == 'ALL' || $(this).val() == '全部'){
				$(".withdrawhistory").find(".depositRecord_contents").css('display','block')
			}
			else if($(this).val() == withdrawarr[i]){
				$(".withdrawhistory").find(".depositRecord_contents")[i].style.display = 'block';	
			}
		}
		//获取到select的选中值
	})
	//全局变量
	//初始化 QRCode
	 
	//弹框消失
	$(".SecretKeycontentimg").on('tap',function(){
	    $(".findout").fadeOut()
		$('.withdrawnumber').html('--')
		$('.withdrawAvailable').html('--');
												
		$('.withdrawFee').html('--');
	})
	//Deposit弹框的显示
	var qrcode = new QRCode(document.getElementById("qrcodecontent"), {	 
	}); 
	var elText = '2223eedasd';
	//deposit Withdraw Asset内容的折叠与展开
	
	
	 //Withdrawpop弹框的显示
	
	
	//提交Withdrawpop弹框中的内容
mui.getExtras(function(extras){
   console.log(extras.type)
   if(extras.type == "wallet"){
	   $(".AssetRortfolio").removeClass("AssetRortfolioblock")
	   $(".AssetRortfolio").eq(0).addClass("AssetRortfolioblock")
	   $(".Asset_Deposit_Withdrawal li").removeClass("AssetDepositWithdrawalcolor")
	    $(".Asset_Deposit_Withdrawal li").eq(0).addClass("AssetDepositWithdrawalcolor")
	   
	  
   }else if(extras.type == "deposit"){
	   $(".AssetRortfolio").removeClass("AssetRortfolioblock")
	   $(".AssetRortfolio").eq(1).addClass("AssetRortfolioblock")
	   $(".Asset_Deposit_Withdrawal li").removeClass("AssetDepositWithdrawalcolor")
	    $(".Asset_Deposit_Withdrawal li").eq(1).addClass("AssetDepositWithdrawalcolor")
	   
   }else if(extras.type == "withdraw"){
	   $(".AssetRortfolio").removeClass("AssetRortfolioblock")
	   $(".AssetRortfolio").eq(2).addClass("AssetRortfolioblock")
	    $(".Asset_Deposit_Withdrawal li").removeClass("AssetDepositWithdrawalcolor")
	    $(".Asset_Deposit_Withdrawal li").eq(2).addClass("AssetDepositWithdrawalcolor")
	  
   }
});	
	//Asset_Deposit_Withdrawal三者内容切换
	$(".Asset_Deposit_Withdrawal li").each(function(){
		$(this).on('tap',function(){
			var i = $(this).index()
			$(".Asset_Deposit_Withdrawal li").removeClass("AssetDepositWithdrawalcolor")
			$(this).addClass("AssetDepositWithdrawalcolor")
			$(".AssetRortfolio").removeClass("AssetRortfolioblock")
			$(".Deposit_selected").removeClass("AssetRortfolioblock")
			$(".AssetRortfolio").eq(i).addClass("AssetRortfolioblock")
			$(".Deposit_selected").eq(i).addClass("AssetRortfolioblock")
		})
	})
	//Asset内容的获取
	function getAssetcontent(){
		//通过后台获取这个地方的数据，所以就不需要写三个content 只需要一个传的html可能会不一样
		var html = ''
		$(".AssetDepositWithdrawalcontent .AssetRortfolio").empty();
		$(".AssetDepositWithdrawalcontent .AssetRortfolio").append(html)
	}
	//Deposit内容的获取
	function getDeposit(){
		//通过后台获取这个地方的数据，所以就不需要写三个content 只需要一个传的html可能会不一样
		var html = ''
		$(".AssetDepositWithdrawalcontent .AssetRortfolio").empty();
		$(".AssetDepositWithdrawalcontent .AssetRortfolio").append(html)
	}
	//Withdrawa内容的获取
	function getWithdrawa(){
		//通过后台获取这个地方的数据，所以就不需要写三个content 只需要一个传的html可能会不一样
		var html = ''
		$(".AssetDepositWithdrawalcontent .AssetRortfolio").empty();
		$(".AssetDepositWithdrawalcontent .AssetRortfolio").append(html)
	}
	//关于搜索
	//Total Asset
	function getTotalAsset(){
		$("#TotalAssetUsd").text();
		$("#TotalAssetUsdrate").text();
		//Pending deposit内容
		$("#Pendingdeposit").empty()
		$("#Pendingdeposit").append()
	}
})