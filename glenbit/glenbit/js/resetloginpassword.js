$(function(){
	var username = localStorage.getItem('username');
	var userarea = localStorage.getItem('userarea');
	var userphone = username.slice(userarea.length);
	var reg = RegExp(/@/);
	if(reg.test(username) == false){
		$.ajax({
			url:'https://rest.glenbit.com/account/try_reset_password_phone',
			type:'post',
			dataType: 'json',
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			data: {
				countryCode: userarea,
				phone: userphone
			},
			success:function(data){
				console.log(data);
				loginstatus(data)
				if(data.errors != undefined){
					if(data.errors[0].code == 9998){
						mui.toast("Api rate limit reached!")
					}
				}else{
					mui.toast(get_lan('pleaseremember'))
				}
			}
		})
	}else if(reg.test(username) == true){
		$.ajax({
			url:'https://rest.glenbit.com/account/try_reset_password',
			type:'post',
			data: {
				email: username
			},
			success:function(data){
				console.log(data);
				loginstatus(data)
				if(data.errors != undefined){
					if(data.errors[0].code == 9998){
						mui.toast("Api rate limit reached!")
					}
				}else{
					mui.toast(get_lan('pleaseremember'))
				}
			}
		})
	}
	document.getElementsByClassName("buttonsubmit")[0].addEventListener('tap', function() {
		//表单中的四个值
		var sendData = {} 
		var verificationcode = document.getElementById("verificationcode").value;
		sendData["verificationcode"] = verificationcode
		//校验输入框不为空,并且复选框为true的情况下
		if(verificationcode!=''){
			//提交部分数据部分
			mui.openWindow({ //目标页面
				url: 'ResetLoginpasswordEnterEmailGetcode.html',
				extras: {
					name:verificationcode,
					version: reg.test(username),
					}
			})
		}else{
			//输入框不符合要求的情况下进行处理
			mui.toast(get_lan("Enter_verification_Code"))
		}
	})
	document.getElementById("gobackpages").addEventListener('tap', function() {
		mui.openWindow({ //目标页面
			url: 'my.html'
		})
	})
	$("#verificationcode").attr("placeholder",get_lan("Enter_verification_Code"))
})