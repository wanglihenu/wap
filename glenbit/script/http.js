//接口
//公共部分
var baseUrl = 'http://192.168.0.103/Flame4d'
function async(url,params,methods){
	//把方法转成小写
	var method = methods.toLowerCase() 
	var pro = {}
	if(method == 'get'){ 
		mui.ajax(baseUrl + url,{
			dataType:'json',
			type: method, 
			headers:{'Content-Type':'application/json'},
			success:function(data){
				//服务器返回的响应，，根据响应结果，来处理数据
				pro ["data"] = data
			},
			error:function(xhr,type,errorThrown){
			//异常处理
				pro ["error"] = errorThrown			
			}	
		}) 
	}
	else if(method == 'post'){
		mui.ajax(baseUrl+url,{
			dataType:'json',
			type: method, 
			data:params,
			headers:{'Content-Type':'application/json'},
			success:function(data){
				pro ["data"]=data
				//服务器返回的响应，，根据响应结果，来处理数据
			},
			error:function(xhr,type,errorThrown){
			//异常处理
				pro ["error"] = errorThrown			
			}	
		}) 
	}else{
		mui.ajax(baseUrl + url,{
			dataType:'json',
			type: method, 
			headers:{'Content-Type':'application/json'},
			success:function(data){
				//服务器返回的响应，，根据响应结果，来处理数据
				pro ["data"] = data
			},
			error:function(xhr,type,errorThrown){
			//异常处理
				pro ["error"] = errorThrown			
			}	
		}) 
	}
	return pro
}
//AssetDepositWithdrawal.html下的接口
//资产
function getTotalAssetUsd(value){
	return async('url',value,'get') 
}
//资产记录
function getTotalAssetUsd(){
	return async('url',value,'get')
}
//Asset Rortfolio
function getAssetRortfolio(){
	return async('url',value,'get')
}
//Asset Rortfolio 中的其它接口
//Deposit
function getWalletDeposit(){
	//Deposit Address
	return async('url',value,'get')
}
//Withdraw
function getWalletWithdraw(){
	//Withdraw
	return async('url',value,'get')
}
//Deposit Records 
function getDepositRecords(){
	return async('url',value,'get')
}
//Deposit Records – Operation 中的其它                                                                       接口
function getDepositRecordsOperation(){
	return async('url',value,'get')
}
//Withdrawal Records
function getWithdrawalRecords(){
	return async('url',value,'get')
}
//Withdrawal Records – Operation 中的其它 
function getWithdrawalRecordsOperation(){
	return async('url',value,'get')
}
//个人中心
//Last login
function getLastlogin(){
	return async('url',value,'get')
}
//APISetting
function getApiSetting(){
	return async('url',value,'get')
}
//APISetting 下的Secret Key
function getSecretKey(){
	return async('url',value,'get')
}
//Create API key
function getCreateAPIkey(){
	return async('url',value,'get')
}
//Delete
function getDelete(){
	return async('url',value,'get')
}
//Resetemailaddres Verify
function ResetemailaddresVerify(){
	return async('url',value,'get')
}
//Resetemailaddres Confirm Email
function ResetemailaddresConfirmEmail(){
	return async('url',value,'get')
}
//Resetemailaddres Confirm Email send code
function ResetemailaddresSendCode(){
	return async('url',value,'get')
}
//Resetemailaddres Enter new mail
function ResetemailaddresEnternewmail(){
	return async('url',value,'get')
}
