//正则表达校验
//校验邮箱
//邮箱@前缀的几种类型：
//1、纯数字       　　123456@qq.com
//2、纯字母 　　　　 lisi@qq.com
//3、字母数字混合  　lisi1234@qq.com
//4、带点的 　　　　 li.si@qq.com
//5、带下划线 　　　 li_si@qq.com
// 6、带连接线　　　 li-si@qq.com
//      邮箱@后缀的类型：
// 1、123456@qq.com
// 2、123456@qq.qq.com
//*至少有两处单词
//*顶级域名一般为2~4位（如cn、com、org 、net）
let reEmail = /^\w+@[a-zA-Z0-9]{2,10}(?:\.[a-z]{2,4}){1,3}$/;
//手机号校验
let rePhone = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/;
//验证n为数字
let reNumber = /^\d{4}$/
//校验ip ipv4
let reIp = /^(?=(\b|\D))(((\d{1,2})|(1\d{1,2})|(2[0-4]\d)|(25[0-5]))\.){3}((\d{1,2})|(1\d{1,2})|(2[0-4]\d)|(25[0-5]))(?=(\b|\D))$/;
// ipv6 /^([\\da-fA-F]{1,4}:){7}([\\da-fA-F]{1,4})$/
// 密码校验 必须含数字字符特殊字符，长度4~16位之间
let rePassword =  /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[~!@#$%^&*()_+`\-={}:";'<>?,.\/]).{4,16}$/; 
//除输入框中所有的空格和禁止输入空格的方法 
function Trim(str,is_global) { 
	var result; 
	result = str.replace(/(^\s+)|(\s+$)/g,""); 
	if(is_global.toLowerCase()=="g") { 
		result = result.replace(/\s/g,""); 
	} 
	console.log(result)
	return result; 
}
// 把日子字符串转换为Date类型的对象 日期字符串格式为 yyyy-MM-DD 或者yyyy-MM-dd HH:mm:ss
let date = new Date('2018-12-31 12:00:59')
// 转换为时间戳 Date类型的数据转换为时间戳
let t1 = date.getTime(date);
let t2 = date.valueOf(date);
//js将当前日期转换为时间戳
let timeStamp = new Date().getTime()
// js将时间戳转换为日期
let dates = new Date(timeStamp);//时间戳为10位需要*1000,时间戳为13位的话不需要乘1000
let yyyy = dates.getFullYear()+"-";
let MM = (dates.getMonth()+1<10?'0'+(dates.getMonth()+1):dates.getMonth()+1)+'-';
let dd = dates.getDate()+ ' ';
let HH = dates.getHours()+':';
let min= dates.getMinutes()+':';
let ss = dates.getSeconds(); 

