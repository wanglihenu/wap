
$(function(){
	
	$.ajax({
		url:'https://rest.glenbit.com/referral/info',
		type:'POST',
		dataType:'json',
		xhrFields: {
			withCredentials: true
		},
		crossDomain: true,
		success:function(data){
			loginstatus(data);
// 			var qrcode = new QRCode(document.getElementById("qrcodecontent"), {
// 				
// 			}); 
 			var elText = 'https://www.glenbit.com/account/signup?ref=' +  data.referralCode;
// 			qrcode.makeCode(elText);
			$("#qrcodecontent").qrcode(
		         {
	             render : "canvas",    //设置渲染方式，有table和canvas，使用canvas方式渲染性能相对来说比较好
	             text : elText,    //扫描二维码后显示的内容,可以直接填一个网址，扫描二维码后自动跳向该链接
                 width : "200",            // //二维码的宽度
                 height : "200",              //二维码的高度
                 background : "#ffffff",       //二维码的后景色
                 foreground : "#000000",        //二维码的前景色
                 src: '../images/ic_yaoqing_logo.png'             //二维码中间的图片
	              }
	        );
			$('.mylinkcontent').html(elText);
			$('.myinvaidnum').html(data.referralCode);
			$('.myProportion').html(data.commissionRatio * 100 + '%');
			var clipboard = new ClipboardJS('.mylinkcopy', {
				text: function() {
					return elText;
				}
			});
			clipboard.on('success', function(e) {
				mui.toast(get_lan('SUCCESS'));
			});
			clipboard.on('error', function(e) {
				mui.toast(get_lan('FAILURE'));
			});
		}
	});
	//返利估值
	$.ajax({
			url:'https://rest.glenbit.com/market_prices',
			type:'GET',
			dataType:'json',
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			success:function(data){
				var rateone = '';
				var ratetwo = '';
				var mybalance = '';
				var ratearr = [];
				var rateto = [];
				for(var i = 0;i < data.length;i++){
					ratearr.push(data[i]);
					if(data[i].from == 'BTC' && data[i].to == 'USDT'){
						rateone = data[i].price;
					}
					if(data[i].from == 'USDT' && data[i].to == 'USD'){
						ratetwo = data[i].price;
					}	
				}
				$('.btcusdrate').html((Number(rateone) * Number(ratetwo)).toFixed(2));
				$.ajax({
					url:'https://rest.glenbit.com/referral/commissions',
					type:'POST',
					dataType:'json',
					xhrFields: {
						withCredentials: true
					},
					crossDomain: true,
					success:function(data){
						//console.log(data);
						loginstatus(data);
						var Variouscoin = '';
						var Variouscointwo = 0;
						if(data.errors == undefined){
							if(data == ''){
								$('.myValuation').html('0.000000')
							}else{
								for(var i = 0 ; i < res.length; i++){
									if(res[i].type == 'AsInviter'){
										//console.log(data[i]);
										var totalassets = Number(res[i].amount);
				// 						//console.log(totalassets)
										for(var j = 0; j < ratearr.length; j++){
											//console.log(ratearr[j])
											if(res[i].coin == ratearr[j].from){
												//console.log(ratearr[j].to)
												if(ratearr[j].to == 'USD'){
												Variouscoin = totalassets * ratearr[j].price;
												}else if(ratearr[j].to == 'USDT'){
												Variouscoin = totalassets * ratearr[j].price * ratetwo;
												}else if(ratearr[j].to == 'BTC'){
												Variouscoin = totalassets * ratearr[j].price * rateone *  ratetwo;
												}
											}
										}	
										Variouscointwo += Number(Variouscoin);
									}
								}
								$('.myValuation').html(Variouscointwo / (rateone *  ratetwo))
							}
						}
					}
				})
			}
		});	
})