
	document.getElementById('gophonesign').addEventListener('tap',function(){
		document.getElementById('goemailsignbox').style.display = 'none';
		document.getElementById('gophonesignbox').style.display = 'block';
	});
	document.getElementById('goemailsign').addEventListener('tap',function(){
		document.getElementById('goemailsignbox').style.display = 'block';
		document.getElementById('gophonesignbox').style.display = 'none';
	});
	document.getElementById('signareacode').addEventListener('tap',function(){
		document.getElementById('signareatips').style.left = '0';
	});
	document.getElementById('signareaback').addEventListener('tap',function(){
		document.getElementById('signareatips').style.left = '100%';
	});
	document.getElementById('gobacklogin').addEventListener('tap',function(){
		mui.openWindow({
			url: 'login.html'
		})
	})
	document.getElementById('useragreementtwo').addEventListener('tap',function(){
		if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('lan') == null || localStorage.getItem('lan') == undefined){
			mui.openWindow({
				url: 'userAgreement.html'
			})
		}else{
			mui.openWindow({
				url: 'userAgreementEn.html'
			})
		}
	});
	document.getElementById('useragreement').addEventListener('tap',function(){
		if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('lan') == null || localStorage.getItem('lan') == undefined){
			mui.openWindow({
				url: 'userAgreement.html'
			})
		}else{
			mui.openWindow({
				url: 'userAgreementEn.html'
			})
		}
	});
///
	


 $(function(){
		$('.signemail').attr('placeholder',get_lan("EMAIL_PLZ"))
		$('.signpassword').attr('placeholder',get_lan("Please_fill_password"))
		$('.signpasswordtwo').attr('placeholder',get_lan("CONFIRM_PASSWORD_PLZ"))
		$('.phonesignemail').attr('placeholder',get_lan("Eenter_Phone_Number"))
		$('.phonesmscode').attr('placeholder',get_lan("Enter_verification_Code"))
		$('.phonesignpassword').attr('placeholder',get_lan("Please_fill_password"))
		$('.phonesignpasswordtwo').attr('placeholder',get_lan("CONFIRM_PASSWORD_PLZ"))
		$('.ReferralID').attr('placeholder',get_lan("Optional"))
		$('.ReferralIDtwo').attr('placeholder',get_lan("Optional"))
		$.ajax({
			url: "../json/countries.json", // 加随机数防止缓存
			type: "get",
			dataType: "json",
			success: function (data) {
				var html = '';
				var regex1 = /\((\d+)\)/;   // () 小括号
				for(var i = 0 ; i < data.length;i++){
					html+="<li class='arealist'>"+data[i].countryCode.displayValue+"<span class='arealistcontent' style='display:none'>"+data[i].countryCode.rawValue+"</span></li>";
				}
				document.getElementById("areaul").innerHTML = html;
				$(".arealist").each(function(){					
					$(this).on('tap',function(){
						var str = $(this).html();
						var strtwo = str.substring(str.indexOf("(")+1,str.indexOf(")"))
						$(".areanumber").html(strtwo);
						$(".areaselect").html($(this).find('.arealistcontent').html());
						document.getElementById('signareatips').style.left = '100%';
						//$(".SecretKey").fadeIn()
					})
				})
			}
			
		})
 	})
	document.getElementsByClassName("protocolbox")[0].addEventListener('tap',function(){
		if(this.checked == true){
			signchecked = false;
			document.getElementsByClassName("protocolboxbg")[0].classList.remove("protocolboxbgtrue");    //删除未勾选选背景图
			document.getElementsByClassName("protocolboxbg")[0].classList.add("protocolboxbgfalse"); 
			//$("label").addClass("iCheck-ed");    //添加勾选态背景图
		}else{
			signchecked = true;
			document.getElementsByClassName("protocolboxbg")[0].classList.remove("protocolboxbgfalse");    //删除未勾选选背景图
			document.getElementsByClassName("protocolboxbg")[0].classList.add("protocolboxbgtrue"); 
		}
	})
	document.getElementsByClassName("protocolbox")[1].addEventListener('tap',function(){
		if(this.checked == true){
			phonesignchecked = false;
			document.getElementsByClassName("protocolboxbg")[1].classList.remove("protocolboxbgtrue");    //删除未勾选选背景图
			document.getElementsByClassName("protocolboxbg")[1].classList.add("protocolboxbgfalse"); 
			//$("label").addClass("iCheck-ed");    //添加勾选态背景图
		}else{
			phonesignchecked = true;
			document.getElementsByClassName("protocolboxbg")[1].classList.remove("protocolboxbgfalse");    //删除未勾选选背景图
			document.getElementsByClassName("protocolboxbg")[1].classList.add("protocolboxbgtrue"); 
		}
	})
	var signemail = '';
	var signpassword = '';
	var signpasswordtwo = '';
	var phonesmscode = '';
	var areanumber ='';
	var signchecked = false;
	var phonesignchecked = false;
	var tabnumber = true;
	var ReferralID = '';
	var signlanguage = 'zh_CN';
		//
	var gtlanguage = 'zh-cn';
	if(localStorage.getItem('lan') == undefined || localStorage.getItem('lan') == 'cn'){
		gtlanguage = 'zh-cn';
	}else if(localStorage.getItem('lan') == 'en'){
		gtlanguage = 'en';
	};
	$(function(){
		$.ajax({
		// 获取id，challenge，success（是否启用failback）
			url: " https://rest.glenbit.com/gt/register?t=" + (new Date()).getTime(), // 加随机数防止缓存
			type: "get",
			dataType: "json",
			success: function (data) {
				// 使用initGeetest接口
				// 参数1：配置参数
				// 参数2：回调，回调的第一个参数验证码对象，之后可以使用它做appendTo之类的事件
				initGeetest({
					gt: data.gt,
					challenge: data.challenge,
					offline: !data.success, // 表示用户后台检测极验服务器是否宕机
					new_captcha: data.new_captcha, // 用于宕机时表示是新验证码的宕机
					product: "bind", // 产品形式，包括：float，popup
					width: "300px",
					https: true,
					lang:gtlanguage,
					new_captcha: true
				}, handlerPopup);
			}
		});
	})
    // 代码详细说明
 var handlerPopup = function (captchaObj) {
     // 注册提交按钮事件，比如在登陆页面的登陆按钮
      captchaObj.onReady(function () {
           $("#wait").hide();
        }).onSuccess(function () {
             var result = captchaObj.getValidate();
             if (!result) {
                 return mui.toast('请完成验证');
             }
			 if(tabnumber == true){
				$.ajax({
					 url: 'https://rest.glenbit.com/account/signup',
					 type: 'POST',
					 dataType: 'json',
					 data: {
						 email: signemail,
						 password: signpassword,
						 lang: signlanguage,
						 geetest_challenge: result.geetest_challenge,
						 geetest_validate: result.geetest_validate,
						 geetest_seccode: result.geetest_seccode,
						 referralCode:ReferralID,
					 },
					 success: function (data) {
						if(data.errors != undefined){
							mui.toast(data.errors[0].msg);
						}else{
							mui.toast(data)
						}
					 }
				 }); 
			 }else if(tabnumber == false){
				 $.ajax({
					 url: 'https://rest.glenbit.com/account/signup_phone',
					 type: 'POST',
					 dataType: 'json',
					 data: {
						 countryCode: areanumber,
						 phone: signemail,
						 password: signpassword,
						 code: phonesmscode,
						 geetest_challenge: result.geetest_challenge,
						 geetest_validate: result.geetest_validate,
						 geetest_seccode: result.geetest_seccode,
						 referralCode:ReferralID,
					 },
					 success: function (data) {
						 if(data.errors != undefined){
							 mui.toast(data.errors[0].msg) 
						 }else{
							 mui.toast(get_lan('SIGNUP_SUCCESS'))
						 }
					 }
				 }); 
			 }
         });
         document.getElementById('signbtn').addEventListener('tap',function () {
			 tabnumber = true;
			 if(localStorage.getItem('lan') == 'cn' || localStorage.getItem('lan') == null){
				 signlanguage = 'zh_CN';
			 }else{
				 signlanguage = 'en_US';
			 }
             // 调用之前先通过前端表单校验
			signemail = $('.signemail').val();
			signpassword = $('.signpassword').val();
			signpasswordtwo = $('.signpasswordtwo').val();
			ReferralID = $('.ReferralID').val();
			if(signemail == ''){
				//alert('邮箱不能为空');get_lan("EMAIL_PLZ")
				mui.toast(get_lan("EMAIL_PLZ"));
				return
			}else if(signemail.indexOf('@') == -1){
				mui.toast(get_lan("EMAIL_INVALID"));
				return
			}else if(signpassword == ''){
				//alert('密码不能为空');
				mui.toast('get_lan("PASSWORD_PLZ")');
				return
			}else if(signpassword.length < 6){
				//alert('密码不能为空');
				mui.toast(get_lan("PASSWORD_INVALID"));
				return
			}
			else if(signpassword != signpasswordtwo){
				//alert('密码输入不一致');
				mui.toast(get_lan("CONFIRM_PASSWORD_INVALID"));
				return
			}else if(signchecked != true){
				//alert('请勾选用户协议');
				mui.toast(get_lan("useragree"));
				return
			}else{
				captchaObj.verify();
			}
         });
		document.getElementById('phonesignbtn').addEventListener('tap',function () {
			tabnumber = false;
			
		     // 调用之前先通过前端表单校验
			areanumber = $(".areaselect").html();
			signemail = $('.phonesignemail').val();
			phonesmscode = $('.phonesmscode').val();
			signpassword = $('.phonesignpassword').val();
			signpasswordtwo = $('.phonesignpasswordtwo').val();
			ReferralID = $('.ReferralIDtwo').val();
			if(signemail == ''){
				mui.toast(get_lan("Eenter_Phone_Number"));
				return
			}else if(phonesmscode == ''){
				mui.toast(get_lan("Enter_verification_Code"));
				return
			}else if(signpassword == ''){
				mui.toast(get_lan("PASSWORD_PLZ"));
				return
			}
			else if(signpassword.length < 6){
				//alert('密码不能为空');
				mui.toast(get_lan("PASSWORD_INVALID"));
				return
			}else if(signpassword != signpasswordtwo){
				mui.toast(get_lan("CONFIRM_PASSWORD_INVALID"));
				return
			}else if(phonesignchecked != true){
				mui.toast(get_lan("useragree"));
				return
			}else{
				captchaObj.verify();
			}
		 });
		 captchaObj.onClose(function () {
			captchaObj.reset(); 
        // 用户把验证关闭了，这时你可以提示用户需要把验证通过后才能进行后续流程
		});
         // 更多接口说明请参见：http://docs.geetest.com/install/client/web-front/
     };
	$('.sendareacode').on('tap',function(){
      	var phone = $(".phonesignemail").val();
		var phonecode = $(".areaselect").html();
		var flag = false;  
		var message = "";
    	 	if(phone == ''){  
				mui.toast(get_lan("Eenter_Phone_Number"));
			  }else{  	
			  	var verificationbtntime=60;
                 $('.sendareacode').attr("disabled",true); 
                 var $disabled = $(".sendareacode").prop("disabled");
               		if ($disabled == true) {
						$.ajax({
							url:"https://rest.glenbit.com/account/sms_verify_code",
							type: 'POST',
							dataType: 'json',
							data: {
								 countryCode: phonecode,
								 phone: phone
							},
							success:function(data){
								if(data.errors != undefined){
									mui.toast(data.errors[0].msg);
								}else{
									mui.toast(get_lan('pleaseremember'))
								}
							}
						})
                     //validCode=false;
                     var t=setInterval(function() {
                         verificationbtntime--;
                         $(".sendareacode").text(verificationbtntime+"s");
                         if (verificationbtntime==0) {
                             clearInterval(t);
                         $(".sendareacode").text("Send");
                              $('.sendareacode').attr("disabled",false); 
                         }
                     },1000)
                }
			  }
      })
    

///

