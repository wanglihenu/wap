$(function(){
	document.getElementById("gobackpage").addEventListener('tap', function() {
		mui.openWindow({ //目标页面
			url: 'googleVerification.html'
		})
	})
	$("#verificationcode").attr("placeholder",get_lan("inputGooglecode"))
	var phonecode = '';
	 function render_target(extras){
		  // target.innerHTML = 'name的值为' + data.name
		  phonecode=extras.phonecode.replace("%40","@");
			$.ajax({
				url:"https://rest.glenbit.com/account/two_step_verify_code",
				type: 'POST',
				dataType: 'json',
				data: {
					code: phonecode,
				},
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success:function(data){
					//console.log(data.errors);
					loginstatus(data)
					if(data.errors != undefined){
						mui.toast(get_lan("Verification_Code_Error"))
					}else{
						$('.privatekey').html(data.key);
						var qrcode = new QRCode(document.getElementById("qrcodecontent"), {	 
						
						}); 
						
						var elText = data.url;
						qrcode.makeCode(elText);
					}
					
				}
			});
		}
	mui.getExtras(function(extras){
		   render_target(extras);
	});

	
	//提交表单
	document.getElementsByClassName("buttonsubmit")[0].addEventListener('tap', function() {
		//表单中的一个值
		var sendData = {} 
		var verificationcode = document.getElementById("verificationcode").value;
		var privatekey = document.getElementById("privatekey").value;
		sendData["verificationcode"] = verificationcode;
		sendData["privatekey"] = privatekey;
		//校验输入框不为空,并且复选框为true的情况下
		if((verificationcode!='')&&(privatekey!='')){
			//提交部分数据部分
			$.ajax({
				url:"https://rest.glenbit.com/account/enable_two_step",
				type: 'POST',
				dataType: 'json',
				data: {
					token: verificationcode,
				},
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				success:function(data){
					loginstatus(data);
					if(data.errors != undefined){
						mui.toast(data.errors[0].msg)
					}else{
						mui.openWindow({ //目标页面
							url: 'my.html'
						})
					}
					
				}
			});
		}else{
			mui.toast(get_lan("Enter_verification_Code"));
			//输入框不符合要求的情况下进行处理 
		} 
	})
	
})