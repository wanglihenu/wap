//websocketdemo
//<h2>WebSocket Test</h2>  
//<div id="output"></div> 
 	var wsUrl ="ws://echo.websocket.org/"; 
    var output;  
    var lockReconnect = false; //避免重复连接 
	var websocket;
	var tt;
    function init() {
    	  output = document.getElementById("output"); 
    	try{
    		websocket= new WebSocket(wsUrl);
			testWebSocket();
    	}catch(e){
    		console.log('catch');
			reconnect(wsUrl);
    	}     
    }  
    function testWebSocket() {  
        //打开通讯
        websocket.onopen = function(evt) { 
        	console.log('33')
            onOpen(evt) 
            //心跳检测重置
            sendHeart()
			websocket.send(JSON.stringify({
				event: 'web_depth',
				'pair': 'BTC_CNY',
				'depth': 1
			}));
        }; 
        //关闭通讯
        websocket.onclose = function(evt) { 
            onClose(evt) 
            console.log('链接关闭');
			reconnect(wsUrl);
        }; 
        //发送消息
        websocket.onmessage = function(evt) { 
            onMessage(evt) 
        }; 
        //通讯错误
        websocket.onerror = function(evt) { 
            onError(evt) 
            console.log('发生异常了');
			reconnect(wsUrl);
        }; 
    } 
    function reconnect(url) {
		if(lockReconnect) {
			return;
		};
		lockReconnect = true;
		//没连接上会一直重连，设置延迟避免请求过多
//		tt && clearTimeout(tt);
//		tt = setTimeout(function() {
//			init(url)
//			lockReconnect = false;
//		}, 4000);
	}
 	//心跳检测
	function sendHeart() {
		websocket.send("HeartBeat");
//		setTimeout(res => {
//			sendHeart()
//		}, 60000)
	}
//	var heartCheck = {
//		timeout: 3000,
//		timeoutObj: null,
//		serverTimeoutObj: null,
//		start: function() {
//			console.log('start');
//			var self = this;
//			this.timeoutObj && clearTimeout(this.timeoutObj);
//			this.serverTimeoutObj && clearTimeout(this.serverTimeoutObj);
//			this.timeoutObj = setTimeout(function() {
//				//这里发送一个心跳，后端收到后，返回一个心跳消息，
//				console.log('55555');
//				websocket.send("HeartBeat");
//				self.serverTimeoutObj = setTimeout(function() {
//					console.log(111);
//					websocket.close();	
//				}, self.timeout);
//			}, this.timeout)
//		}
//	}
    function onOpen(evt) { 
        writeToScreen("CONNECTED"); 
        doSend("WebSocket rocks"); 
    }  
 
    function onClose(evt) { 
        writeToScreen("DISCONNECTED"); 
    }  
 
    function onMessage(evt) { 
        writeToScreen('<span style="color: blue;">RESPONSE: '+ evt.data+'</span>'); 
        websocket.close(); 
    }  
 
    function onError(evt) { 
        writeToScreen('<span style="color: red;">ERROR:</span> '+ evt.data); 
    }  
 
    function doSend(message) { 
        writeToScreen("SENT: " + message);  
        websocket.send(message); 
    }  
 
    function writeToScreen(message) { 
        var pre = document.createElement("p"); 
        pre.style.wordWrap = "break-word"; 
        pre.innerHTML = message; 
        output.appendChild(pre); 
    }  
 
    window.addEventListener("load", init, false);